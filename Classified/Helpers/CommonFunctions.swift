//
//  CommonFunctions.swift
//  Classified
//
//  Created by WC_Macmini on 29/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import Foundation
import UIKit
import ClassifiedWebkit

struct storyBrdIds {
    static let homeView = "HomeViewController_id"
    static let homeDetailVc = "HomeDetailViewController_id"
    static let profileVC = "ProfileViewController_id"
    static let settingsVc = "SavedViewController_id"
    static let loginVC = "LoginViewControllerId"
    static let subscriptionVC  = "SubscriptionViewController_id"
    static let notificationVC = "NotificationViewController_id"
    static let uploadVC = "UploadViewController_id"
    static let filterVC = "FilterViewController_id"
    static let searchVC = "SearchViewController_id"
    static let productDetailVc  = "ProductDetailViewController_id"
    static let reviewVC = "ReviewViewController_id"
    static let signUpVc = "SignUpViewController_id"
    static let forPassVC = "ForgotPasswordViewController_id"
    static let languageVC = "WelcomeViewController_id"
    static let editProfileVC = "EditProfileViewController_id"
    static let changePassVC = "ChangePasswordViewController_id"
    static let additionalSettingsVC = "AdditionalSettingsViewController_id"
    static let indicatorStryBrdId = "activity_ind_stryBrd_id"
    static let subCategoryView = "SubCategoryViewController_id"
    static let profilesettingsVc = "SettingsViewController_id"
     static let accountDetailsVC = "AccoutDetailsViewController_id"
    static let webViewVC = "WeViewViewController_id"
    static let paymentVC  = "PaymentViewController_id"
}


var categoryIdCommon : Int?

func showAlert(title:String,subTitle:String,sender:UIViewController){
    let alert = UIAlertController(title: title , message: subTitle, preferredStyle: .alert)
    let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
,style: .cancel) { (alert) -> Void in
    }
    alert.addAction(OkButton)
    sender.present(alert, animated: true, completion: nil)
}

func showNetworkError(sender:UIViewController){
    let alert = UIAlertController(title: "" , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "network_error", comment: ""), preferredStyle: .alert)
    let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
,style: .cancel) { (alert) -> Void in
    }
    alert.addAction(OkButton)
    sender.present(alert, animated: true, completion: nil)
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}


func showUploadView(sender:UIViewController){
    if ClassifiedAPIFacade.shared.session?.token == nil{
                                    let alert = UIAlertController(title: "" , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_login", comment: "")
    , preferredStyle: .alert)
                                    let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "login", comment: "")
    ,style: .default) { (alert) -> Void in
                                        showLogin(sender: sender)
                                    }
                                    alert.addAction(OkButton)
                                    let cancel = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "")
    ,style: .cancel) { (alert) -> Void in
                                                   }
                                    alert.addAction(cancel)
                                    
                                    sender.present(alert, animated: true, completion: nil)
                                    return
    }
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(identifier: storyBrdIds.uploadVC)
    vc.modalPresentationStyle = .fullScreen
    sender.present(vc, animated: true, completion: nil)
}



func showSearchView(sender:UIViewController){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(identifier: storyBrdIds.searchVC)
    sender.present(vc, animated: true, completion: nil)
}

func animationScaleEffect(view:UIView)
   {
       view.transform = view.transform.scaledBy(x: 0.001, y: 0.001)
      UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
           view.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
       }, completion: nil)

   }

func showLogin(sender:UIViewController){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(identifier: storyBrdIds.loginVC) as? LoginViewController
    vc?.modalPresentationStyle = .fullScreen
    sender.present(vc!, animated: true, completion: nil)
}

func saveUserDetails(userDetails:User){
    let dict = userDetails.dicValue()
    UserDefaults.standard.set(dict, forKey: "user_details")
    UserDefaults.standard.synchronize()
}

func getUserDetails() -> [String:Any]?{
    return UserDefaults.standard.dictionary(forKey: "user_details") ?? nil
}

func deleteUserDetails(){
    let defaults = UserDefaults.standard
    defaults.set(nil, forKey: "user_details")
    defaults.synchronize()
}

func setImage(imageURL:String) -> URL{
    let urlString = imageURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    let imagepath =  URL(string:ClassifiedAPIConfig.BaseUrl.imageBaseServerpath+urlString)
    return imagepath!
}

func checkFirstTime() -> Bool{
    let userD = UserDefaults()
    let value = userD.bool(forKey: "isFirstTime")
    return value
}

func checkRegisterdDevice() -> Bool{
    let userD = UserDefaults()
    let value = userD.bool(forKey: "registerdDevice")
    return value
}

func registerdDevice (){
    let userD = UserDefaults()
    userD.set(true, forKey: "registerdDevice")
}

func changeFirstTime (){
    let userD = UserDefaults()
    userD.set(true, forKey: "isFirstTime")
}

extension UIColor {
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        switch chars.count {
        case 3: chars = chars.flatMap { [$0, $0] }; fallthrough
        case 6: chars = ["F","F"] + chars
        case 8: break
        default: return nil
        }
        self.init(red: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
                  green: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
                  blue: .init(strtoul(String(chars[6...7]), nil, 16)) / 255,
                  alpha: .init(strtoul(String(chars[0...1]), nil, 16)) / 255)
    }
}

extension String {

    var htmlToAttributedString: NSAttributedString? {
        guard
            let data = self.data(using: .utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [
                NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
                ], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }

    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

class ScaledHeightImageView: UIImageView {

override var intrinsicContentSize: CGSize {
    if let myImage = self.image {
        let myImageWidth = myImage.size.width
        let myImageHeight = myImage.size.height
        let myViewWidth = self.frame.size.width
        
        let ratio = myViewWidth/myImageWidth
        let scaledHeight = myImageHeight * ratio
        
        return CGSize(width: myViewWidth, height: scaledHeight)
    }
    
    return CGSize(width: -1.0, height: -1.0)
}
}

