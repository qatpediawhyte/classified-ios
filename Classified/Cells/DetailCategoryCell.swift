//
//  DetailCategoryCell.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class DetailCategoryCell: UICollectionViewCell {
     @IBOutlet var imgItem: UIImageView!
     @IBOutlet var lblName: UILabel!
     @IBOutlet var lblCount: UILabel!
}
