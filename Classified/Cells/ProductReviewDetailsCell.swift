//
//  ProductReviewDetailsCell.swift
//  Classified
//
//  Created by WC_Macmini on 01/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class ProductReviewDetailsCell: UITableViewCell {

    struct const {
        static let reviewListId = "review_list_cell_id"
    }
    
    @IBOutlet var tblReview: UITableView!
    
    var dataArray = [Review]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    func setUpTable(){
        tblReview?.delegate = self
        tblReview?.dataSource = self
        if tblReview != nil{
            tblReview.reloadData()
        }
        
    }
    
    private func setAttributedTexts(name:String,text:String) -> NSMutableAttributedString{

        let attributedText = NSMutableAttributedString(string:"\(name ) " , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12 , weight: .medium)])
        attributedText.append(NSAttributedString(string: text , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12,weight: .light)]))
        return attributedText
    }
    
}

extension ProductReviewDetailsCell : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: const.reviewListId, for: indexPath) as! ReviewListCell
        cell.selectionStyle = .none
        cell.txtReview.attributedText = setAttributedTexts(name: dataArray[indexPath.row].user_name ?? "", text: dataArray[indexPath.row].review ?? "")
        cell.txtReview.sizeToFit()
        cell.imgReview.kf.setImage(with: setImage(imageURL: dataArray[indexPath.row].user_photo ?? ""))
        return cell
    }
    
}
