//
//  PrductDetailsCell.swift
//  Classified
//
//  Created by WC_Macmini on 01/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class PrductDetailsCell: UITableViewCell {

    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblSpecification: UILabel!
    @IBOutlet var lblModel: UILabel!
    @IBOutlet var lblBrand: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
