//
//  ExpandableHeader.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import LUExpandableTableView

class ExpandableHeader: LUExpandableTableViewSectionHeader {
    @IBOutlet weak var expandCollapseButton: UIButton!
    @IBOutlet weak var label: UILabel!
    
    override var isExpanded: Bool {
           didSet {
               // Change the title of the button when section header expand/collapse
            expandCollapseButton?.setImage(isExpanded ? UIImage(named: "up_arrow") : UIImage(named: "down_arraow"), for: .normal)
           }
       }
       
       // MARK: - Base Class Overrides
       
       override func awakeFromNib() {
           super.awakeFromNib()
           
           label?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnLabel)))
           label?.isUserInteractionEnabled = true
       }
       
       // MARK: - IBActions
       
       @IBAction func expandCollapse(_ sender: UIButton) {
           // Send the message to his delegate that shold expand or collapse
           delegate?.expandableSectionHeader(self, shouldExpandOrCollapseAtSection: section)
       }
       
       // MARK: - Private Functions
       
       @objc private func didTapOnLabel(_ sender: UIGestureRecognizer) {
           // Send the message to his delegate that was selected
           delegate?.expandableSectionHeader(self, wasSelectedAtSection: section)
       }

}
