//
//  SubCategoryListCell.swift
//  Classified
//
//  Created by WC_Macmini on 22/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class SubCategoryListCell: UITableViewCell {
    
    @IBOutlet var lblCategory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
