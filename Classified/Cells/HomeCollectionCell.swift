//
//  HomeCollectionCell.swift
//  Classified
//
//  Created by WC_Macmini on 29/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {
     @IBOutlet var imgType: UIImageView!
     @IBOutlet var lblType: UILabel!
}
