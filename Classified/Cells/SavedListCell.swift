//
//  SavedListCell.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class SavedListCell: UITableViewCell {

      @IBOutlet var lblName: UILabel!
      @IBOutlet var lblTime: UILabel!
      @IBOutlet var lblPrice: UILabel!
      @IBOutlet var lblSellerName: UILabel!
      @IBOutlet var btnCall: UIButton!
      @IBOutlet var imgItem: UIImageView!
      @IBOutlet var imgWishList: UIImageView!
     @IBOutlet var lblUsedCondition: UILabel!
     @IBOutlet var btnComment: UIButton!
    
    typealias callTapped = (SavedListCell) -> Void
    var callTappeddAction : callTapped?
    
    typealias commentTapped = (SavedListCell) -> Void
    var commentTappedAction : commentTapped?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnComment.layer.borderWidth = 1
        btnComment.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        
          if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            lblName.textAlignment = .right
            lblTime.textAlignment = .right
            lblPrice.textAlignment = .right
            lblSellerName.textAlignment = .right
          }else{
            lblName.textAlignment = .left
            lblTime.textAlignment = .left
            lblPrice.textAlignment = .left
            lblSellerName.textAlignment = .left
        }
    }

    func setCallAction(handler:callTapped?) {
        callTappeddAction = handler
    }
    
    func setcommentAction(handler:commentTapped?) {
           commentTappedAction = handler
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCall(_ sender: UIButton) {
        animationScaleEffect(view: sender)
        callTappeddAction!(self)
    }
    
    @IBAction func btnComment(_ sender: UIButton) {
        animationScaleEffect(view: sender)
        commentTappedAction!(self)
    }
        
}
