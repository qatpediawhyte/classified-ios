//
//  PorudtSellerDetailsCell.swift
//  Classified
//
//  Created by WC_Macmini on 01/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class PorudtSellerDetailsCell: UITableViewCell {

    @IBOutlet var btnWriteReview: UIButton!
    @IBOutlet var btnViewProfile: UIButton!
    @IBOutlet var lblReviewHeading: UILabel!
    @IBOutlet var lnlMember: UILabel!
    @IBOutlet var viewMember: UIView!
    @IBOutlet var lblMemberSince: UILabel!
    @IBOutlet var lblSellerName: UILabel!
    @IBOutlet var imgSellerName: UIImageView!
    @IBOutlet var lblDellserDetailsHeading: UILabel!
    @IBOutlet var lblDetailsHeading: UILabel!
    @IBOutlet var lblDetails: UILabel!
    
    typealias reviewTapped = (PorudtSellerDetailsCell) -> Void
    var reviewTappedAction : reviewTapped?
    
    typealias viewProfileTapped = (PorudtSellerDetailsCell) -> Void
    var viewProfileTappedAction : viewProfileTapped?
       
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func putStar(_ sender: Any) {
         reviewTappedAction!(self)
    }
    
    func reviewTappedAction(handler:reviewTapped?) {
           reviewTappedAction = handler
    }
    
    func viewProfileTappedActions(handler:reviewTapped?) {
           viewProfileTappedAction = handler
    }

    @IBAction func btnViewProfile(_ sender: Any) {
        viewProfileTappedAction!(self)
    }
    
    @IBAction func btnWriteReview(_ sender: Any) {
        
    }
}
