//
//  NotificationListCell.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class NotificationListCell: UITableViewCell {

   
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var imgItem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

