//
//  SubscriptionListCell.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class SubscriptionListCell: UITableViewCell {

    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDetails: UITextView!
    @IBOutlet var lblHEading: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
