//
//  ProductBasicCell.swift
//  Classified
//
//  Created by WC_Macmini on 01/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ImageSlideshow
import HCSStarRatingView

class ProductBasicCell: UITableViewCell {
    
    @IBOutlet var btnCall: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var productSlideshow: ImageSlideshow!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblViewCount: UILabel!
    @IBOutlet var lblDaysCount: UILabel!
    @IBOutlet var lblSellerName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblIsNew: UILabel!
    @IBOutlet var btnWish: UIButton!
    @IBOutlet var imgProduct: ScaledHeightImageView!
    @IBOutlet var lblSpecHading: UILabel!
    @IBOutlet var btnViewDetails: UIButton!
     @IBOutlet var profileStack: UIStackView!
    
      @IBOutlet var btnEdit: UIButton!
      @IBOutlet var btnDelete: UIButton!
      @IBOutlet var btnPromote: UIButton!
    
    @IBOutlet var btnRate: UIButton!
    @IBOutlet var rating: HCSStarRatingView!
    @IBOutlet var lblRating: UILabel!
    
    var isAdded = false
    
    typealias callTapped = (ProductBasicCell) -> Void
    var callTappeddAction : callTapped?
    
    typealias wishListTapped = (ProductBasicCell) -> Void
    var wishListTappeddAction : callTapped?
    
    typealias editTapped = (ProductBasicCell) -> Void
    var editTappedAction : editTapped?
    
    typealias deleteTapped = (ProductBasicCell) -> Void
    var deleteTappedAction : deleteTapped?
    
    typealias promoteTapped = (ProductBasicCell) -> Void
    var promoteTappedAction : promoteTapped?
    
    typealias commentTapped = (ProductBasicCell) -> Void
    var commentTappedAction : commentTapped?
    
    typealias ratingTapped = (ProductBasicCell) -> Void
    var ratingTappedAction : ratingTapped?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnComment.layer.borderWidth = 1
        btnComment.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        
        btnEdit.layer.borderWidth = 1
        btnEdit.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        
        btnDelete.layer.borderWidth = 1
        btnDelete.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        
        btnPromote.layer.borderWidth = 1
        btnPromote.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        
    }
    
    func setCallAction(handler:callTapped?) {
        callTappeddAction = handler
    }
    
    func wishListAction(handler:promoteTapped?) {
        wishListTappeddAction = handler
    }
    
    func editAction(handler:editTapped?) {
           editTappedAction = handler
    }
    
    func deleteAction(handler:deleteTapped?) {
            deleteTappedAction = handler
    }
    
    func prmoteAction(handler:editTapped?) {
               promoteTappedAction = handler
       }
    
    func commentAction(handler:commentTapped?) {
            commentTappedAction = handler
    }
    
    func ratingAction(handler:ratingTapped?) {
            ratingTappedAction = handler
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCall(_ sender: UIButton) {
        animationScaleEffect(view: sender)
          callTappeddAction!(self)
    }
    
    @IBAction func btnComment(_ sender: UIButton) {
        animationScaleEffect(view: sender)
        commentTappedAction!(self)
    }
    
    @IBAction func btnAddToWishList(_ sender: UIButton) {
        animationScaleEffect(view: sender)
        wishListTappeddAction!(self)
//        if isAdded == false{
//            isAdded = true
//            sender.setImage(UIImage(named: "wishRed_big"), for: .normal)
//        }else{
//             isAdded = false
//            sender.setImage(UIImage(named: "wishBlack_big"), for: .normal)
//        }
    }
    
    @IBAction func btnEdit(_ sender: UIButton) {
        editTappedAction!(self)
    }
    
    @IBAction func btnDelete(_ sender: UIButton) {
        deleteTappedAction!(self)
    }
    
    @IBAction func btnPromote(_ sender: UIButton) {
        promoteTappedAction!(self)
    }
    
    @IBAction func btnRate(_ sender: UIButton) {
        ratingTappedAction!(self)
    }
    
}
