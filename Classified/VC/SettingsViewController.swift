//
//  SettingsViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class SettingsViewController: UIViewController {

    struct VCConst {
        static let settingCellId = "settings_cell_id"
    }
    
    @IBOutlet var imgName: UIImageView!
    @IBOutlet var lblMember: UILabel!
    @IBOutlet var lblName: UILabel!
    
    var itemsArray = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.extendedLayoutIncludesOpaqueBars = true
        
        if (getUserDetails()?["active_subscription"] ?? "") as? Int == 0{
           itemsArray =  [["image":"profile_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "profile", comment: "")
            ],
                                    ["image":"subscriptions_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "subscription", comment: "")
            ],
            //                        ["image":"savedItems_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "saved_item", comment: "")
            //],
                                    ["image":"notifications_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: "")
            ],
                                    ["image":"settings_icon_side","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "settings", comment: "")
            ],
                                    ["image":"admin","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "admin", comment: "")
                                    ],
                                    ["image":"logout_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "log_out", comment: "")
            ]]
        }else{
            itemsArray =  [["image":"profile_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "profile", comment: "")
                       ],
                                               ["image":"subscriptions_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "subscription", comment: "")
                       ],
                                               ["image":"account_details","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "account_details", comment: "")
                       ],
                                               ["image":"notifications_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: "")
                       ],
                                               ["image":"settings_icon_side","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "settings", comment: "")
                       ],
                                               ["image":"admin","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "admin", comment: "")
                                               ],
                                               ["image":"logout_icon","name":LocalizationSystem.sharedInstance.localizedStringForKey(key: "log_out", comment: "")
                       ]]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if self.navigationController != nil{
            navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        }
        
        lblName.text = (getUserDetails()?["name"] ?? "") as? String
        lblMember.text = (getUserDetails()?["mamber_since"] ?? "") as? String
        
        if getUserDetails()?["photo"] as? String != "" && getUserDetails()?["photo"] as? String != nil{
             imgName.kf.setImage(with: setImage(imageURL: getUserDetails()?["photo"] as? String ?? ""))
        }else{
          self.imgName.image = UIImage(named: "profile_thumb")
        }
        
       
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        if self.navigationController == nil{
            self.dismiss(animated: true, completion: nil)
        }else{
          self.navigationController?.popViewController(animated: true)
        }
          
      }
    
}

extension SettingsViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCConst.settingCellId, for: indexPath) as! SettingsListCell
        cell.lblName.text = itemsArray[indexPath.row]["name"]
        cell.imgItem.image = UIImage(named:itemsArray[indexPath.row]["image"] ?? "")
        if itemsArray[indexPath.row]["name"] == "Log Out"{
            cell.imgRight.isHidden = true
        }else{
            cell.imgRight.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (getUserDetails()?["active_subscription"] ?? "") as? Int == 0{
            switch indexPath.row {
                    case 0:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.editProfileVC) as? EditProfileViewController
                        vc?.modalPresentationStyle = .fullScreen
                        self.present(vc!, animated: true, completion: nil)
                    case 1:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.subscriptionVC) as? SubscriptionViewController
                        vc?.modalPresentationStyle = .fullScreen
                        self.present(vc!, animated: true, completion: nil)
            //        case 2:
            //            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.settingsVc) as? SavedViewController
            //            vc?.isFromSettings = true
            //            self.present(vc!, animated: true, completion: nil)
                    case 2:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.notificationVC) as? NotificationViewController
                        vc?.isFromSettings = true
                        self.present(vc!, animated: true, completion: nil)
                    case 3:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.additionalSettingsVC) as? AdditionalSettingsViewController
                        self.present(vc!, animated: true, completion: nil)
                    case 4:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.webViewVC) as? WeViewViewController
                        vc?.modalPresentationStyle = .fullScreen
                        self.present(vc!, animated: true, completion: nil)
                    case 5:
                         ClassifiedAPIFacade.shared.resetSession()
                         deleteUserDetails()
                         showLogin(sender: self)
                    default:
                        print("")
                    }
        }else{
            switch indexPath.row {
                    case 0:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.editProfileVC) as? EditProfileViewController
                        vc?.modalPresentationStyle = .fullScreen
                        self.present(vc!, animated: true, completion: nil)
                    case 1:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.subscriptionVC) as? SubscriptionViewController
                        vc?.modalPresentationStyle = .fullScreen
                        self.present(vc!, animated: true, completion: nil)
                    case 2:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.accountDetailsVC) as? AccoutDetailsViewController
                        self.present(vc!, animated: true, completion: nil)
                    case 3:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.notificationVC) as? NotificationViewController
                        vc?.isFromSettings = true
                        self.present(vc!, animated: true, completion: nil)
                    case 4:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.additionalSettingsVC) as? AdditionalSettingsViewController
                        self.present(vc!, animated: true, completion: nil)
                    case 5:
                        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.webViewVC) as? WeViewViewController
                        vc?.modalPresentationStyle = .fullScreen
                        self.present(vc!, animated: true, completion: nil)
                    case 6:
                         ClassifiedAPIFacade.shared.resetSession()
                         deleteUserDetails()
                         showLogin(sender: self)
                    default:
                        print("")
                    }
        }
        
    }
}


//if let vc = self.parent?.parent as? UITabBarController{
//
//    vc.selectedIndex = 4
//let navigation =  vc.children[3] as? UINavigationController
//let vcs = navigation?.viewControllers[0] as? ProfileViewController
////vcs?.isFromSideMenu = true
//
//for m in vc.children{
//if let nav = m as? UINavigationController{
//nav.popToRootViewController(animated: false)
//}
//    }
//self.dismiss(animated: true, completion: nil)
//    }

extension SettingsViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
}
