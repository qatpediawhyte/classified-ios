//
//  ChangePasswordViewController.swift
//  Classified
//
//  Created by WC_Macmini on 03/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class ChangePasswordViewController: UIViewController {

    @IBOutlet var btnCOnfirm: UIButton!
    @IBOutlet var txtConfirmPass: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var lblChangePass: UILabel!
     @IBOutlet var txtOldPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLanguage()
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnConfirm(_ sender: Any) {
        if txtConfirmPass.text != txtPassword.text{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "password_mismatch", comment: "")
, sender: self)
            return
        }
        performChangePass()
        
    }
    
    func setLanguage(){
        lblChangePass.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "change_pass", comment: "")
        txtOldPass.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "old_pass", comment: "")
        txtPassword.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new_pass", comment: "")
        txtConfirmPass.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirm_pass", comment: "")
        btnCOnfirm.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirm", comment: ""), for: .normal)
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            txtOldPass.textAlignment = .right
            txtPassword.textAlignment = .right
            txtPassword.textAlignment = .right
        }else{
           txtOldPass.textAlignment = .left
            txtPassword.textAlignment = .left
            txtPassword.textAlignment = .left
        }
    }
    
    func performChangePass(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .overFullScreen
        loadingview?.loadWith(parent: self)

        var detail = [String:Any]()
        detail["oldPassword"] = txtOldPass.text ?? ""
        detail["newPassword"] = txtPassword.text ?? ""
        detail["confirmPassword"] = txtConfirmPass.text ?? ""
        
        ClassifiedAPIFacade.shared.chnagePassword(details: detail) {(aResp) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aResp?.statusCode == 200{
                if aResp?.json?["status"].intValue == 200{
                    saveUserDetails(userDetails: ClassifiedAPIFacade.shared.session!.currentUser!)
                    let alert = UIAlertController(title: "" , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "profile_updated", comment: "")
, preferredStyle: .alert)
                    let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
,style: .cancel) { (alert) -> Void in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alert.addAction(OkButton)
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                }
            }else{
               showNetworkError(sender: self)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
