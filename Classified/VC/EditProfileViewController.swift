//
//  EditProfileViewController.swift
//  Classified
//
//  Created by WC_Macmini on 03/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var txtCOntact: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var txtNAme: UITextField!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var btnSave: UIButton!
    
    var imagePicker = UIImagePickerController()
    var isInsideView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtCOntact.delegate = self
        txtNAme.text = (getUserDetails()?["name"] ?? "") as? String
        txtEmail.text = (getUserDetails()?["email"] ?? "") as? String
        txtCOntact.text = (getUserDetails()?["phone"] ?? "") as? String
        imgUser.kf.setImage(with: setImage(imageURL: getUserDetails()?["photo"] as? String ?? ""))
        setLanguage()
    }
    
    func setLanguage(){
        lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_profile", comment: "")
        btnSave.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
        txtNAme.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "name", comment: "")
        txtEmail.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "email", comment: "")
        txtCOntact.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            txtNAme.textAlignment = .right
            txtEmail.textAlignment = .right
            txtCOntact.textAlignment = .right
        }else{
            txtNAme.textAlignment = .left
            txtEmail.textAlignment = .left
            txtCOntact.textAlignment = .left
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
         if txtNAme.text == ""{
                         showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_name", comment: "")
, sender: self)
                         return
                     }else if txtEmail.text == ""{
                         showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_email", comment: "")
, sender: self)
                         return
                     }else if txtEmail.text != ""{
                         let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
                         let emailTest = NSPredicate(format: "SELF MATCHES %@", emailReg)
                         guard emailTest.evaluate(with: txtEmail.text) == true else {
                             let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "invalid_email", comment: "")
 , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_valid_emal", comment: "")
, preferredStyle: .alert)
                             let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
,style: .cancel) { (alert) -> Void in
                             }
                             alert.addAction(OkButton)
                             self.present(alert, animated: true, completion: nil)
                             return
                         }
                     }
              if txtCOntact.text == ""{
                  showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_phone", comment: "")
, sender: self)
                  return
              }
              
              performUPdate()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSelectImage(_ sender: UIButton) {
        let alertController = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "upload_profile", comment: "")
, message: "", preferredStyle: .actionSheet)
        
        let Camera = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "use_camera", comment: "")
, style: .default) { action in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }else{
                let alert = UIAlertController(title: "" , message: "CameraNotFound", preferredStyle: .alert)
                let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
                }
                alert.addAction(OkButton)
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        
        let Gallery = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "use_gallery", comment: "")
, style: .default){ action in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                    
                }            }else
            {
                let alert = UIAlertController(title: "", message: "Can'tLocatePhotoLibrary", preferredStyle: .alert)
                let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
                }
                alert.addAction(OkButton)
                self.present(alert, animated: true, completion: nil)
                
            }
            
            
        }
        let Cancel = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "")
, style: .default)
        alertController.addAction(Camera)
        alertController.addAction(Gallery)
        alertController.addAction(Cancel)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
             self.isInsideView = true
            imgUser.image = image
        }
        else if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
             self.isInsideView = true
            imgUser.image = image
        }
        else{
            print("error")
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
          return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
      }
      
      // Helper function inserted by Swift 4.2 migrator.
      fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
          return input.rawValue
      }

    
    func performUPdate(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .overFullScreen
        loadingview?.loadWith(parent: self)

        var detail = [String:Any]()
        detail["name"] = txtNAme.text ?? ""
        detail["phone"] = txtCOntact.text ?? ""
        detail["email"] = txtEmail.text ?? ""
        
        if imgUser.image != nil {
           // let resize = resizeImage(image: imgUser.image!, targetSize: CGSize(width: 640, height: 480))

            if let imgData:Data = imgUser.image?.pngData() {
                detail["photo"] = imgData
            }
        }
        else {
            detail["photo"] = ""
        }
        
        ClassifiedAPIFacade.shared.updateProfile(details:detail) {(aResp) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aResp?.statusCode == 200{
                if aResp?.json?["status"].intValue == 200{
                    saveUserDetails(userDetails: ClassifiedAPIFacade.shared.session!.currentUser!)
                    let alert = UIAlertController(title: "" , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "profile_updated", comment: "")
, preferredStyle: .alert)
                    let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
,style: .cancel) { (alert) -> Void in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alert.addAction(OkButton)
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                   showNetworkError(sender: self)
                }
            }else{
               showNetworkError(sender: self)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditProfileViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        switch textField {
        case txtCOntact :
            return prospectiveText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                prospectiveText.count <= 8
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true;
    }
    
    
}
