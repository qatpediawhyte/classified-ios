//
//  SearchViewController.swift
//  Classified
//
//  Created by WC_Macmini on 01/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class SearchViewController: UIViewController {

    struct VCConst {
           static let savedListId = "saved_listCell_id"
       }
    
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tblPost:  UITableView!
    
    var keyWord = ""
    var postList = [Post]()
    var selectedProductId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtSearch.becomeFirstResponder()
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(textField : UITextField){
          self.getPostList()
    }
    
    func getPostList(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
                
        var bannerDetails = [String:Any]()
        
        bannerDetails["keywords"] =  txtSearch.text
        

        
        ClassifiedAPIFacade.shared.lisPosts(details: bannerDetails) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.postList.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Post(json: m)
                    self.postList.append(booking)
                }
                    }
                
                UIView.transition(with: self.tblPost, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblPost.reloadData()}, completion: nil)
                
                let label = UILabel()
                label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_products", comment: "")
                               
                label.textAlignment = .center
                label.textColor = UIColor.darkGray
                label.sizeToFit()
                label.frame = CGRect(x: self.tblPost.frame.width/2, y: self.tblPost.frame.height/2, width: self.tblPost.frame.width, height: 50)
                               
                if self.postList.count == 0{
                    self.tblPost.backgroundView = label
                }else{
                    self.tblPost.backgroundView = nil
                }
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    
    @IBAction func btnClose(_ sender: UIButton) {
        txtSearch.text = ""
        self.getPostList()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        if self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
           self.dismiss(animated: true, completion: nil)
        }
    }
}

extension SearchViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postList.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: VCConst.savedListId,  for: indexPath) as! SavedListCell
        cell.selectionStyle = .none

        if postList[indexPath.row].wish_list == 0{
            cell.imgWishList.image = UIImage(named: "wishList_black")
        }else{
            cell.imgWishList.image = UIImage(named: "wishListRed")
        }
        
        cell.lblName.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? postList[indexPath.row].name_ar : postList[indexPath.row].name ?? "" //arabic
        cell.lblTime.text = postList[indexPath.row].time_agos ?? "" //arabic
        cell.lblSellerName.text = postList[indexPath.row].user_name ?? ""
        if postList[indexPath.row].product_condition == 0{
             cell.lblUsedCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
        }else{
             cell.lblUsedCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: "")
        }
       
        let urlString = postList[indexPath.row].photo?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! ?? ""
              let imagepath =  URL(string:ClassifiedAPIConfig.BaseUrl.imageBaseServerpath+urlString)
        cell.imgItem.kf.setImage(with: imagepath)
        
        var previousPrice = ""
        var price = ""
        
        postList[indexPath.row].previous_price =  nil
        
        if postList[indexPath.row].previous_price != nil && postList[indexPath.row].previous_price != "0" &&  postList[indexPath.row].previous_price != ""{
            previousPrice = postList[indexPath.row].previous_price!  + "QAR "
            
            if postList[indexPath.row].price != nil{
               price = postList[indexPath.row].price!  + "QAR "
            }
         
            let attributedText = NSMutableAttributedString(string:previousPrice , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12 , weight: .light)])
                  attributedText.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedText.length))
                  attributedText.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.darkGray, range: NSMakeRange(0, attributedText.length))

                  
                       attributedText.append(NSAttributedString(string: price, attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16 , weight: .medium)]))
                  cell.lblPrice.attributedText = attributedText
        }else{
            if postList[indexPath.row].price != nil{
               price = postList[indexPath.row].price!  + "QAR "
            }
            
            cell.lblPrice.text = "\(price)"
        }
        
        
        
        
      
        
        cell.btnCall.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "call", comment: ""), for: .normal)
        
        cell.setcommentAction { (aCell) in
            let vc = self.storyboard?.instantiateViewController(identifier:storyBrdIds.reviewVC ) as! ReviewViewController
            vc.postId = self.postList[indexPath.row].post_id!
            vc.postedUserId = self.postList[indexPath.row].user_id!
            vc.isFromList = true
             vc.modalPresentationStyle = .fullScreen
             vc.postName = (LocalizationSystem.sharedInstance.getLanguage() == "ar" ? self.postList[indexPath.row].name_ar :  self.postList[indexPath.row].name) ?? ""
            if self.navigationController == nil{
              self.present(vc, animated: true, completion: nil)
            }else{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        cell.setCallAction { (aCell) in

            if let url = URL(string: "tel://\(self.postList[indexPath.row].phone_number ?? "")"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.productDetailVc) as! ProductDetailViewController
        selectedProductId = postList[indexPath.row].post_id!
        vc.selectedProductID = selectedProductId
         vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
