//
//  NotificationViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ExpandableButton
import ClassifiedWebkit

class NotificationViewController: UIViewController {

    struct VCConst {
           static let notifCellId = "notification_list_cellId"
       }
    
  
    @IBOutlet var btnWidth: NSLayoutConstraint!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var tblNotif: UITableView!
    @IBOutlet var lblNoData: UILabel!
    
    var isFromSettings = false
    var isFirstTime = true
    var today = ["","",""]
    var thisWeek = ["","","","","",""]
    var notifArray = [Notifications]()
    override func viewDidLoad() {
        super.viewDidLoad()

        if isFromSettings == true{
            btnBack.isHidden = false
        }else{
            btnBack.isHidden = true
        }

        lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: "")

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isFirstTime = true
        self.btnClose.backgroundColor = UIColor.white
        self.btnClose.layer.masksToBounds = false
        self.btnClose.setImage(UIImage(named: "close_icon"), for: .normal)
        self.btnClose.setTitle("", for: .normal)
        self.btnWidth.constant = 50
        getNotificationList()
    }
    
    func deleteNotification(notfcnID:Int){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.deleteNotification(notfcId: notfcnID ) { (aResp) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aResp?.statusCode == 200{
                if aResp?.json?["status"].intValue == 200{
                    //self.getNotificationList()
                }else{
                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                }
            }else{
              showNetworkError(sender: self)
            }
        }
    }
    
    func deleteAll(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.deleteAllNotification { (aResp) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aResp?.statusCode == 200{
                if aResp?.json?["status"].intValue == 200{
                    
                }else{
                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                }
            }else{
              showNetworkError(sender: self)
            }
        }
    }
    
    func getNotificationList(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
    
        
        ClassifiedAPIFacade.shared.getnotificationList { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.notifArray.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Notifications(json: m)
                    self.notifArray.append(booking)
                }
                    }
                UIView.transition(with: self.tblNotif, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblNotif.reloadData()}, completion: nil)
                let label = UILabel()
                label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_products", comment: "")
                
                label.textAlignment = .center
                label.textColor = UIColor.darkGray
                label.sizeToFit()
                label.frame = CGRect(x: self.tblNotif.frame.width/2, y: self.tblNotif.frame.height/2, width: self.tblNotif.frame.width, height: 50)
                
                if self.notifArray.count == 0{
                    self.tblNotif.backgroundView = label
                }else{
                    self.tblNotif.backgroundView = nil
                }
                
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        if isFromSettings == true{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        animationScaleEffect(view: sender)
        if isFirstTime == true{
            isFirstTime = false
            self.btnClose.setImage(nil, for: .normal)
            self.btnClose.setTitleColor(UIColor.init(hexString: "#3CBCDD")
                , for: .normal)
            self.btnClose.layer.masksToBounds =  true
            self.btnWidth.constant = 80
            self.btnClose.setTitle("Clear All", for: .normal)
            
        }else{
            notifArray.removeAll()
            UIView.transition(with: self.tblNotif, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblNotif.reloadData()}, completion: nil)
                          let label = UILabel()
                          label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_notification", comment: "")
            label.textAlignment = .center
            label.textColor = UIColor.darkGray
            label.sizeToFit()
            label.frame = CGRect(x: self.tblNotif.frame.width/2, y: self.tblNotif.frame.height/2, width: self.tblNotif.frame.width, height: 50)
                          
            self.tblNotif.backgroundView = label
            self.btnClose.backgroundColor = UIColor.white
            self.btnClose.layer.masksToBounds = false
            self.btnClose.setImage(UIImage(named: "close_icon"), for: .normal)
            self.btnClose.setTitle("", for: .normal)
            self.btnWidth.constant = 50
            deleteAll()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NotificationViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: VCConst.notifCellId, for: indexPath) as! NotificationListCell
        cell.selectionStyle = .none
        cell.lblName.text = notifArray[indexPath.row].text
        cell.lblTime.text = notifArray[indexPath.row].time
        cell.imgItem.kf.setImage(with: setImage(imageURL: notifArray[indexPath.row].photo ?? ""))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if notifArray[indexPath.row].post_id != nil && notifArray[indexPath.row].post_id != 0{
            if self.navigationController != nil{
                let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.productDetailVc) as! ProductDetailViewController
                vc.selectedProductID = notifArray[indexPath.row].post_id
                 vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.productDetailVc) as! ProductDetailViewController
                vc.selectedProductID = notifArray[indexPath.row].post_id
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
//
//        let label = UILabel()
//        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
//        if section == 0{
//             label.text = "Today"
//        }else{
//           label.text = "This Week"
//        }
//        label.font = label.font.withSize(12)
//        label.textColor = UIColor.darkGray
//        headerView.addSubview(label)
//
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
   func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in

               
               // tableView.deleteRows(at: [indexPath], with: .fade)
                self.deleteNotification(notfcnID: self.notifArray[indexPath.row].id ?? 0)
                 self.notifArray.remove(at: indexPath.row)
                 UIView.transition(with: self.tblNotif, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblNotif.reloadData()}, completion: nil)
            }
            deleteAction.image = UIImage(systemName: "trash")
            deleteAction.backgroundColor = .systemRed
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
    }
}
