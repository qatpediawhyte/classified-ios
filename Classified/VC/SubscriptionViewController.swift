//
//  SubscriptionViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit
import StoreKit

class SubscriptionViewController: UIViewController,UIScrollViewDelegate {

    struct VCConst {
        static let subscriptionCellId = "subscription_cell_id"
    }
    
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var tblSubs: UITableView!
    @IBOutlet var imglogo: UIImageView!
    @IBOutlet var pageIndicator: UIPageControl!
    var vSpinner : UIView?
    var subscriptionArray = [Subscription]()
    var subscriptionArraySecond = [Subscription]()
     var refresher:UIRefreshControl!
    var paymentUrl = ""
    
    var products: [SKProduct] = []
    var subScriptionId  = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
         reload()
        getPostList()
         imglogo.isHidden = true
        pageIndicator.currentPage = 0
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.tblSubs.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
        self.tblSubs.addGestureRecognizer(swipeLeft)
        
        lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "subscription", comment: "")
        
        NotificationCenter.default.addObserver(self, selector: #selector(SubscriptionViewController.handlePurchaseNotification(_:)),
                                                  name: .IAPHelperPurchaseNotification,
                                                  object: nil)
    }
    
    @objc func reload() {
       products = []
       let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
              loadingview?.modalPresentationStyle = .fullScreen
              loadingview?.loadWith(parent: self)
       
       RazeFaceProducts.store.requestProducts{ [weak self] success, products in
         guard let self = self else { return }
         if success {
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            self.products = products!
//           DispatchQueue.main.async {
//             self.tableView.reloadData()
//           }
           
         }
         
//         DispatchQueue.main.async {
//           self.refreshControl?.endRefreshing()
//         }
         
       }
     }
    
    
    
    @objc func handlePurchaseNotification(_ notification: Notification) {
        removeSpinner()
        if let alreadyExist = notification.object as? String{
            if alreadyExist == "alreadyExist"{
                return
            }
        }
       guard let productID = notification.object as? String,
         let index = products.index(where: { product -> Bool in
           product.productIdentifier == productID
         })
       else { return }
        
        ClassifiedAPIFacade.shared.subScribeSuccess(subScribeId: subScriptionId ) { (ares) in
            if ares?.statusCode == 200{
                if ares?.json?["status"].intValue == 200{
                    print("success")
                }else{
                   showAlert(title: "", subTitle: ares?.json?["message"].stringValue ?? "", sender: self)
                }
            }else{
               print("failure")
            }
        }

       tblSubs.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
     }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                pageIndicator.currentPage = 0
                  UIView.transition(with: self.tblSubs, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblSubs.reloadData()}, completion: nil)
            case .left:
               pageIndicator.currentPage = 1
                UIView.transition(with: self.tblSubs, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblSubs.reloadData()}, completion: nil)
            default:
                break
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(scrollView.contentOffset.y + scrollView.frame.size.height) <= Int(scrollView.contentSize.height + scrollView.contentInset.bottom) + 210) {
            imglogo.isHidden = true
        }else{
            imglogo.isHidden = false
        }
    }
    
    func getPostList(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
                
        
        ClassifiedAPIFacade.shared.getSubscriptionList { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.subscriptionArray.removeAll()
                self.subscriptionArraySecond.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Subscription(json: m)
                    self.subscriptionArray.append(booking)
                }
                    }
                
                if let data = aRes?.json?["second_page_data"].array{
                    for m in data{
                        let booking = Subscription(json: m)
                        self.subscriptionArraySecond.append(booking)
                    }
                }
                
                UIView.transition(with: self.tblSubs, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblSubs.reloadData()}, completion: nil)
//
//                let label = UILabel()
//                label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_products", comment: "")
//
//                label.textAlignment = .center
//                label.textColor = UIColor.darkGray
//                label.sizeToFit()
//                label.frame = CGRect(x: self.tblPost.frame.width/2, y: self.tblPost.frame.height/2, width: self.tblPost.frame.width, height: 50)
//
//                if self.postList.count == 0{
//                    self.tblPost.backgroundView = label
//                }else{
//                    self.tblPost.backgroundView = nil
//                }
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
             self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnRestore(_ sender: UIButton) {
        RazeFaceProducts.store.restorePurchases()
    }
      
    

}

extension SubscriptionViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pageIndicator.currentPage == 0 ?  subscriptionArray.count : subscriptionArraySecond.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCConst.subscriptionCellId, for: indexPath) as! SubscriptionListCell
        
        if pageIndicator.currentPage == 0{
                cell.lblPrice.text = "\(subscriptionArray[indexPath.row].title ?? "") - \(subscriptionArray[indexPath.row].price ?? "") QAR"
                   let products = "Allowed Products : \(subscriptionArray[indexPath.row].allowed_products ?? "")<br><br>"
                   let banner = "Allowed Banner Products : \(subscriptionArray[indexPath.row].allowed_banner_products ?? "")<br><br>"
                   let days = "Days : \(subscriptionArray[indexPath.row].days ?? "")<br><br>"
                   let description = products + banner + days + (subscriptionArray[indexPath.row].details ?? "")
                   cell.lblDetails.text = description.htmlToString
                   cell.selectionStyle = .none
                   cell.lblDetails.isScrollEnabled = false
                   cell.lblDetails.sizeToFit()
        }else{
            cell.lblPrice.text = "\(subscriptionArraySecond[indexPath.row].title ?? "") - \(subscriptionArraySecond[indexPath.row].price ?? "") QAR"
                   let products = "Allowed Products : \(subscriptionArraySecond[indexPath.row].allowed_products ?? "")<br><br>"
                   let banner = "Allowed Banner Products : \(subscriptionArraySecond[indexPath.row].allowed_banner_products ?? "")<br><br>"
                   let days = "Days : \(subscriptionArraySecond[indexPath.row].days ?? "")<br><br>"
                   let description = products + banner + days + (subscriptionArraySecond[indexPath.row].details ?? "")
                   cell.lblDetails.text = description.htmlToString
                   cell.selectionStyle = .none
                   cell.lblDetails.isScrollEnabled = false
                   cell.lblDetails.sizeToFit()
        }

       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if pageIndicator.currentPage == 0{
             let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                         loadingview?.modalPresentationStyle = .fullScreen
                         loadingview?.loadWith(parent: self)

                   ClassifiedAPIFacade.shared.makeSubscribe(subScribeId: subscriptionArray[indexPath.row].id ?? 0){ (aResp) in
                             DispatchQueue.main.async {
                                 loadingview?.removeFromParentView(self)
                             }
                             if aResp?.statusCode == 200{
                                 if aResp?.json?["status"].intValue == 200{
                                   self.paymentUrl = aResp?.json?["url"].stringValue ?? ""
                                   DispatchQueue.main.async {
                                    self.subScriptionId = self.subscriptionArray[indexPath.row].id ?? 0
                                    RazeFaceProducts.store.buyProduct(self.products[indexPath.row])
                                    self.showSpinner(onView: self.view)
                                   }
                                 }else{
                                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                                 }
                             }else{
                               showNetworkError(sender: self)
                             }
                         }
            
        }else{
            let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                         loadingview?.modalPresentationStyle = .fullScreen
                         loadingview?.loadWith(parent: self)

                   ClassifiedAPIFacade.shared.makeSubscribe(subScribeId: subscriptionArraySecond[indexPath.row].id ?? 0){ (aResp) in
                             DispatchQueue.main.async {
                                 loadingview?.removeFromParentView(self)
                             }
                             if aResp?.statusCode == 200{
                                 if aResp?.json?["status"].intValue == 200{
                                   self.paymentUrl = aResp?.json?["url"].stringValue ?? ""
                                   DispatchQueue.main.async {
                                    self.subScriptionId = self.subscriptionArraySecond[indexPath.row].id ?? 0
                                    RazeFaceProducts.store.buyProduct(self.products[3+indexPath.row])
                                    self.showSpinner(onView: self.view)
                                   }
                                 }else{
                                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                                 }
                             }else{
                               showNetworkError(sender: self)
                             }
                         }
            
        }
        
    }
}


extension SubscriptionViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
