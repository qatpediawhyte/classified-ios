//
//  HomeViewController.swift
//  Classified
//
//  Created by WC_Macmini on 29/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit
import Kingfisher

class HomeViewController: UIViewController,UIScrollViewDelegate {

    struct VCConst {
        static let collectionCellId = "home_collection_cell_id"
    }
    
    @IBOutlet var colCategory: UICollectionView!
    @IBOutlet var txtSeaarch: UITextField!
    @IBOutlet var imglogo: UIImageView!
    
    var allCategories = [Categorys]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imglogo.isHidden = true
        getCategoryList()
        txtSeaarch.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "search", comment: "")
          if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            txtSeaarch.textAlignment = .right
          }else{
            txtSeaarch.textAlignment = .left
        }
        
    }
    
    func getCategoryList(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
                
        ClassifiedAPIFacade.shared.getCategories { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
        //                if aRes?.json?["success"].intValue == 0{
        //                    showAlert(title: "", subTitle: aRes?.json?["msg"].stringValue ?? "", sender: self)
        //                }else{
                self.allCategories.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Categorys(json: m)
                    self.allCategories.append(booking)
                }
                    }
                  UIView.transition(with: self.colCategory, duration: 1.0, options: .curveEaseIn, animations: {self.colCategory.reloadData()}, completion: nil)
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    @IBAction func uploadItem(_ sender: UIButton) {
        showUploadView(sender: self)
    }
    
    @IBAction func searchItem(_ sender: UIButton) {
        showSearchView(sender: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(scrollView.contentOffset.y + scrollView.frame.size.height) <= Int(scrollView.contentSize.height + scrollView.contentInset.bottom) + 210) {
            imglogo.isHidden = true
        }else{
            imglogo.isHidden = false
        }
    }
}

extension HomeViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
  
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width/2-5, height: collectionView.frame.width/2-5+50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        allCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCConst.collectionCellId, for: indexPath) as! HomeCollectionCell
        
        let urlString = allCategories[indexPath.item].photo?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! ?? ""
        let imagepath =  URL(string:ClassifiedAPIConfig.BaseUrl.imageBaseServerpath+urlString)
        cell.imgType.kf.setImage(with: imagepath)
        cell.lblType.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[indexPath.item].name_ar : allCategories[indexPath.item].name //arabic
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if allCategories[indexPath.item].sub_cat_count ?? 0 >= 1{
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.subCategoryView) as! SubCategoryViewController
            categoryIdCommon = allCategories[indexPath.item].id
            vc.heading = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[indexPath.item].name_ar ?? "" : allCategories[indexPath.item].name  ?? ""
             vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.homeDetailVc) as! HomeDetailViewController
            categoryIdCommon = allCategories[indexPath.item].id
            vc.header = (LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[indexPath.item].name_ar : allCategories[indexPath.item].name) ?? ""
             vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
}
