//
//  AccoutDetailsViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class AccoutDetailsViewController: UIViewController {

     @IBOutlet var lblNAme: UILabel!
    @IBOutlet var txtCOntent: UITextView!
    @IBOutlet var viewContent: UIView!
    @IBOutlet var lblHeading: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getPLanDetails()
        viewContent.layer.cornerRadius = 10
        viewContent.layer.masksToBounds = true
        viewContent.layer.borderWidth = 0.5
        viewContent.layer.borderColor = UIColor.lightGray.cgColor
        lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "account_details", comment: "")
    }
    

    func getPLanDetails(){
           let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                 loadingview?.modalPresentationStyle = .fullScreen
                 loadingview?.loadWith(parent: self)
           
           ClassifiedAPIFacade.shared.getPlanDetails { (aRes) in
                     DispatchQueue.main.async {
                         loadingview?.removeFromParentView(self)
                     }
                     if aRes?.statusCode == 200{
                         if let data = aRes?.json?["data"].dictionary{
                            self.lblNAme.text = data["plan_name"]?.stringValue ?? ""
                            let banner = "Allowed Products : \(data["category_banners"]?.stringValue ?? "")\n\n"
                            let media = "Allowed Banner Products : \(data["media_advertisement"]?.stringValue ?? "")\n\n"
                            let day = "Days : \(data["expire_day"]?.stringValue ?? "")\n\n"
                            let date = "Expiry Date : \(data["expire_date"]?.stringValue ?? "")"
                            self.txtCOntent.text = banner + media + day + date
                       }
                     }else{
                         showNetworkError(sender: self)
                     }
                 }
       }
    
    @IBAction func btnBack(_ sender: UIButton) {
           if self.navigationController == nil{
               self.dismiss(animated: true, completion: nil)
           }else{
             self.navigationController?.popViewController(animated: true)
           }
             
         }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
