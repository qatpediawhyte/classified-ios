//
//  AdditionalSettingsViewController.swift
//  Classified
//
//  Created by WC_Macmini on 03/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class AdditionalSettingsViewController: UIViewController {
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var lblChangePass: UILabel!
    @IBOutlet var lblChangeLang: UILabel!
    @IBOutlet var lblPrivacyPolicy: UILabel!
    @IBOutlet var lblTerms: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setLanguage()
    }
    
    func setLanguage(){
        lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "settings", comment: "")
        lblChangePass.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "change_pass", comment: "")
        lblChangeLang.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "change_lang", comment: "")
        lblPrivacyPolicy.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "privacy_policy", comment: "")
        lblTerms.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "terms", comment: "")
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOptions(_ sender: UIButton) {
        switch sender.tag {
        case 10:
            let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.changePassVC) as! ChangePasswordViewController
             vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            break
        case 20:
            let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.languageVC) as! WelcomeViewController
            vc.isFromSettings = true
             vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            break
        case 30:
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.webViewVC) as? WeViewViewController
            vc?.isPrivacy = 1
            vc?.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: true, completion: nil)
            break
        case 40:
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.webViewVC) as? WeViewViewController
            vc?.isPrivacy = 2
            vc?.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: true, completion: nil)
            break
        default:
            print("invalid")
        }
    }
    
}
