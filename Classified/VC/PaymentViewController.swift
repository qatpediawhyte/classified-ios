//
//  PaymentViewController.swift
//  Classified
//
//  Created by WC_Macmini on 16/08/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import WebKit
import ClassifiedWebkit

class PaymentViewController: UIViewController {
    
    @IBOutlet var webView: WKWebView!
    var WebUrl = ""
    let activity = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        let url = URL(string: "http:" + WebUrl)
        webView.load(URLRequest(url: url!))
    }
    
    @IBAction func btnKeepGoing(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PaymentViewController:WKNavigationDelegate{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activity.start()
        print("started")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activity.stop()
        if webView.url?.absoluteString.contains("paypal/success") ?? false {
           let alert = UIAlertController(title: "" , message: "Payment Success", preferredStyle: .alert)
                         let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
                            DispatchQueue.main.async {
                                let homeView = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.homeView)
                                homeView?.modalPresentationStyle = .fullScreen
                                self.present(homeView!, animated: true, completion: nil)
                            }
                         }
                         alert.addAction(OkButton)
                         self.present(alert, animated: true, completion: nil)
        }else if webView.url?.absoluteString.contains("paypal/cancel") ?? false{
            showAlert(title: "", subTitle: "Payment failure", sender: self)
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
}

class ActivityIndicator
{class ActivityIndicator
    {
        var activityView:UIActivityIndicatorView!
        var view = UIView(frame: UIScreen.main.bounds)
        
        func start()
        {
            if activityView == nil
            { activityView = UIActivityIndicatorView(style: .whiteLarge)
              activityView.frame = CGRect(x: (view.bounds.maxX/2)-20,
                                            y: (view.bounds.maxY/2)-20,
                                            width: 40, height: 40)
              self.view.addSubview(activityView)
              self.view.backgroundColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:0.8)
              UIApplication.shared.keyWindow?.addSubview(self.view)
              activityView.startAnimating()
            }
            else if activityView.isAnimating == true
            {
            }
            else
            { UIApplication.shared.keyWindow?.addSubview(self.view)
              activityView.startAnimating()
            }
        }
        
        func stop()
        {
            if activityView == nil || activityView.isAnimating == false
            {
            }
            else
            { activityView.stopAnimating()
              self.view.removeFromSuperview()
            }
        }
    }

    var activityView:UIActivityIndicatorView!
    var view = UIView(frame: UIScreen.main.bounds)
    
    func start()
    {
        if activityView == nil
        { activityView = UIActivityIndicatorView(style: .whiteLarge)
          activityView.frame = CGRect(x: (view.bounds.maxX/2)-20,
                                        y: (view.bounds.maxY/2)-20,
                                        width: 40, height: 40)
          self.view.addSubview(activityView)
          self.view.backgroundColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:0.8)
          UIApplication.shared.keyWindow?.addSubview(self.view)
          activityView.startAnimating()
        }   
        else if activityView.isAnimating == true
        {
        }
        else
        { UIApplication.shared.keyWindow?.addSubview(self.view)
          activityView.startAnimating()
        }
    }
    
    func stop()
    {
        if activityView == nil || activityView.isAnimating == false
        {
        }
        else
        { activityView.stopAnimating()
          self.view.removeFromSuperview()
        }
    }
}
