//
//  FilterViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import LUExpandableTableView

class FilterViewController: UIViewController {

    struct VCConst {
        static let cellId = "filter_list_cell_id"
        static let headerId = "MyExpandableTableViewSectionHeader_id"
    }
    @IBOutlet var filterTable: LUExpandableTableView!
    @IBOutlet var btnClear: UIButton!
    @IBOutlet var btnApply: UIButton!
    @IBOutlet var lblHeading: UILabel!
    
    var prodyctListView = HomeDetailViewController()
    
    var data = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "price", comment: "")
,LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_cond", comment: "")
]
   // var brand = ["Apple","Samsung","Huwai"]
    var price = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "high_low", comment: "")
,LocalizationSystem.sharedInstance.localizedStringForKey(key: "low_high", comment: "")
]
    //var color = ["Blue","Green","Red"]
    //var ratin = ["4 star","5 star","6 star"]
   var condition = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: "")
,LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
]
    
    var selectedPrice = ""
    var selectedConditiin = 0
    
    var selectedFirst = -1
    var selectedSecond = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        filterTable.register(UINib(nibName: "MyExpandableTableViewSectionHeader", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: VCConst.headerId)
        filterTable.expandableTableViewDataSource = self
        filterTable.expandableTableViewDelegate = self
        btnClear.layer.borderWidth = 1
        btnClear.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        
        if prodyctListView.priceFilter == "D_A"{
            selectedFirst = 0
        }else if prodyctListView.priceFilter == "A_D"{
            selectedFirst = 1
        }
        
        if prodyctListView.conditionfilter == "1"{
            selectedSecond = 0
        }else if prodyctListView.conditionfilter == "0"{
            selectedSecond = 1
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
           self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCLear(_ sender: UIButton) {
        prodyctListView.conditionfilter = ""
        prodyctListView.priceFilter = ""
        prodyctListView.getPostList()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnApply(_ sender: UIButton) {
        prodyctListView.conditionfilter = "\(selectedConditiin)"
        prodyctListView.priceFilter = selectedPrice
        prodyctListView.getPostList()
        self.dismiss(animated: true, completion: nil)
    }


}

extension FilterViewController: LUExpandableTableViewDataSource {
   func numberOfSections(in expandableTableView: LUExpandableTableView) -> Int {
        return data.count
   }
   
   func expandableTableView(_ expandableTableView: LUExpandableTableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
        return price.count
    case 1:
        return condition.count
//    case 2:
//        return color.count
//    case 3:
//         return ratin.count
    default:
        return 0
    }
   }
   
   func expandableTableView(_ expandableTableView: LUExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: VCConst.cellId) as? FilterListCell else {
           assertionFailure("Cell shouldn't be nil")
           return UITableViewCell()
       }
    
    switch indexPath.section {
        case 0:
             cell.lblName.text =  price[indexPath.row]
             if selectedFirst == indexPath.row{
                cell.backgroundColor = UIColor.lightGray
             }else{
                cell.backgroundColor = UIColor.white
            }
        case 1:
             cell.lblName.text =  condition[indexPath.row]
        if selectedSecond == indexPath.row{
            cell.backgroundColor = UIColor.lightGray
         }else{
            cell.backgroundColor = UIColor.white
        }
//        case 2:
//             cell.lblName.text =  color[indexPath.row]
//        case 3:
//              cell.lblName.text =  ratin[indexPath.row]
        default:
             cell.lblName.text = ""
        }
       
       
       
       return cell
   }
   
   func expandableTableView(_ expandableTableView: LUExpandableTableView, sectionHeaderOfSection section: Int) -> LUExpandableTableViewSectionHeader {
       guard let sectionHeader = expandableTableView.dequeueReusableHeaderFooterView(withIdentifier: VCConst.headerId) as? ExpandableHeader else {
           assertionFailure("Section header shouldn't be nil")
           return LUExpandableTableViewSectionHeader()
       }
       
       sectionHeader.label.text = data[section]
       
       return sectionHeader
   }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            selectedFirst = indexPath.row
            if indexPath.row == 0{
                 selectedPrice = "D_A"
            }else{
                selectedPrice = "A_D"
            }
        }else{
            selectedSecond = indexPath.row
            if indexPath.row == 0{
                 selectedConditiin = 1
            }else{
                selectedConditiin = 0
            }
        }
        expandableTableView.reloadData()
    }
}

// MARK: - LUExpandableTableViewDelegate

extension FilterViewController: LUExpandableTableViewDelegate {
    func expandableTableView(_ expandableTableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return  0
    }
    
   func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
       return 50
   }
   
   func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
       /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
       return 69
   }
   
   // MARK: - Optional
   

   
   func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectSectionHeader sectionHeader: LUExpandableTableViewSectionHeader, atSection section: Int) {
       print("Did select cection header at section \(section)")
   }
   
   func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       print("Will display cell at section \(indexPath.section) row \(indexPath.row)")
   }
   
   func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplaySectionHeader sectionHeader: LUExpandableTableViewSectionHeader, forSection section: Int) {
       print("Will display section header for section \(section)")
   }
}
