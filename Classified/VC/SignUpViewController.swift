//
//  SignUpViewController.swift
//  Classified
//
//  Created by WC_Macmini on 29/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class SignUpViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var btnKeepGoing: UIButton!
    @IBOutlet var btnResendCode: UIButton!
    @IBOutlet var lblDidntReceiveCode: UILabel!
    @IBOutlet var lblEnterCode: UILabel!
    @IBOutlet var lblVerification: UILabel!
    @IBOutlet var lblCreateAnAccount: UILabel!
    @IBOutlet var lblAlreadyHave: UILabel!
    @IBOutlet var lblSignIn: UILabel!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPass: UITextField!
    @IBOutlet var btnCreate: UIButton!
    @IBOutlet var txtCode: [UITextField]!
    @IBOutlet var viewVarification: UIView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var txtPhone: UITextField!
    
    var loginView = LoginViewController()
    var isInsideView = false
    var imagePicker = UIImagePickerController()
    var imageSelected = false
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.delegate = self
        Bundle.main.loadNibNamed("VarificationCodeView", owner: self, options: nil)
        setLanguage()
    }
    
    func setLanguage(){
        lblCreateAnAccount.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sign_up", comment: "").capitalized
        lblAlreadyHave.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "alredy_have", comment: "")
        lblSignIn.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sign_in", comment: "")
        btnCreate.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "create_account", comment: ""), for: .normal)
        
        txtUserName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "name", comment: "")
        txtEmail.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "email", comment: "")
        txtPhone.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "")
        txtPassword.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "")
        txtConfirmPass.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirm_pass", comment: "")
        
          if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            txtUserName.textAlignment = .right
            txtEmail.textAlignment = .right
            txtPhone.textAlignment = .right
            txtPassword.textAlignment = .right
            txtConfirmPass.textAlignment = .right
          }else{
            txtUserName.textAlignment = .left
            txtEmail.textAlignment = .left
            txtPhone.textAlignment = .left
            txtPassword.textAlignment = .left
            txtConfirmPass.textAlignment = .left
        }
        
    }
    
    func checkPhone() -> Bool{
        let test = txtPhone.text ?? ""
        if test.prefix(1) == "3" || test.prefix(1) == "5" || test.prefix(1) == "6" || test.prefix(1) == "7"{
            return true
        }else{
            return false
        }
        
    }
    
    @IBAction func btnSignUp(_ sender: UIButton) {
        
//        viewVarification.frame = self.view.bounds
//       self.view.addSubview(viewVarification)
//        self.view.bringSubviewToFront(viewVarification)
//       self.viewVarification.transform = self.viewVarification.transform.scaledBy(x: 0.001, y: 0.001)
//
//        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
//            self.viewVarification.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
//       }, completion: nil)

//        for all in txtCode{
//            all.layer.cornerRadius = 5
//            all.layer.masksToBounds = true
//            all.layer.borderWidth = 1
//            all.layer.borderColor =  UIColor.init(hexString: "#3CBCDD")?.cgColor
//            all.addTarget(self, action: #selector(textChanged), for: .editingChanged)
 //       }
        
        
        if txtUserName.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_name", comment: "")
, sender: self)
            return
        }else if txtEmail.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_email", comment: ""), sender: self)
            return
        }else if txtEmail.text != ""{
            let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailReg)
            guard emailTest.evaluate(with: txtEmail.text) == true else {
                let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "invalid_email", comment: "")
 , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_valid_emal", comment: "")
, preferredStyle: .alert)
                let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
,style: .cancel) { (alert) -> Void in
                }
                alert.addAction(OkButton)
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        if txtPhone.text?.count != 8 || checkPhone() == false{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_valid_phone", comment: "")
            , sender: self)
                       return
        }else if txtPassword.text == ""{
           showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_password", comment: "")
, sender: self)
           return
        }else if txtPassword.text != txtConfirmPass.text{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "password_mismatch", comment: "")
, sender: self)
            return
        }else if imageSelected == false{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "upload_profile", comment: "")
            , sender: self)
                        return
        }
        perFormSignUP()
        
    }
    
    func perFormSignUP(){
        var profDic = [String:Any]()
        profDic["name"] = txtUserName.text
        profDic["email"] = txtEmail.text
        profDic["phone"] = txtPhone.text
        profDic["password"] = txtPassword.text
        
        if imgUser.image != nil {
           // let resize = resizeImage(image: imgUser.image!, targetSize: CGSize(width: 640, height: 480))

            if let imgData:Data = imgUser.image!.pngData() {
                profDic["photo"] = imgData
            }
        }
        else {
            profDic["photo"] = ""
        }
        

        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .overFullScreen
        loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.signUp(profDet: profDic) { (aResp) in
           
            if aResp?.statusCode == 200{
                if aResp?.json?["status"].intValue == 200{
                          var profDic = [String:Any]()
                          profDic["email"] = self.txtEmail.text
                          profDic["password"] = self.txtPassword.text
                               
                         ClassifiedAPIFacade.shared.login(loginDet:profDic) { (aRes) in
                              DispatchQueue.main.async {
                                             loadingview?.removeFromParentView(self)
                                         }
                          if aRes?.statusCode == 200{
                             if aRes?.json?["status"].intValue == 200{
                                  saveUserDetails(userDetails: (ClassifiedAPIFacade.shared.session?.currentUser)!)
                                  self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                              }else{
                                 showAlert(title: "Oops!", subTitle: aRes?.json?["msg"].stringValue ?? "", sender: self)
                              }
                              }else{
                                  showNetworkError(sender: self)
                              }
                          }
                    
                }else{
                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                }
            }else{
              showNetworkError(sender: self)
            }
        }
    }
    
    @IBAction func btnSignin(_ sender: UIButton) {
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnResendCode(_ sender: Any) {
    }
    
    @IBAction func btnUploadImage(_ sender: Any) {
        let alertController = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ChangeCountry", comment: "")
, message: "", preferredStyle: .actionSheet)
        
        let Camera = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "use_camera", comment: "")
, style: .default) { action in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }else{
                let alert = UIAlertController(title: "" , message: "CameraNotFound", preferredStyle: .alert)
                let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
                }
                alert.addAction(OkButton)
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        
        let Gallery = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "use_gallery", comment: "")
, style: .default){ action in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                    
                }            }else
            {
                let alert = UIAlertController(title: "", message: "Can'tLocatePhotoLibrary", preferredStyle: .alert)
                let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
                }
                alert.addAction(OkButton)
                self.present(alert, animated: true, completion: nil)
                
            }
            
            
        }
        let Cancel = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "")
, style: .default)
        alertController.addAction(Camera)
        alertController.addAction(Gallery)
        alertController.addAction(Cancel)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
             self.isInsideView = true
            imgUser.image = image
            imageSelected = true
        }
        else if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
             self.isInsideView = true
            imgUser.image = image
            imageSelected = true
        }
        else{
            print("error")
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
          return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
      }
      
      // Helper function inserted by Swift 4.2 migrator.
      fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
          return input.rawValue
      }
    
    @IBAction func btnKeepGoing(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func textChanged(sender: UITextField) {
        if sender.text?.count ?? 0 > 0 {
            let nextField = sender.superview?.viewWithTag(sender.tag + 1) as? UITextField
            nextField?.becomeFirstResponder()
        }
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension SignUpViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        switch textField {
        case txtPhone :
            return prospectiveText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                prospectiveText.count <= 8
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true;
    }
    
    
}

extension String{
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
    }
}
