//
//  ExploreViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class ExploreViewController: UIViewController {

    @IBOutlet var pageIndicator: UIPageControl!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var colCategory: UICollectionView!
    @IBOutlet var colExplore: UICollectionView!
    @IBOutlet var txtSeaarch: UITextField!

    struct VCConst {
        static let headerListCellId = "expore_header_cell_id"
        static let listCellId = "explore_list_cell_id"
    }
    
    var selectedItem = 0
    var allCategories = [Categorys]()
    var postList = [Post]()
    var postListSecond = [Post]()
    var selectedCat = 0
    var refresher:UIRefreshControl!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategoryList()
        
        colExplore.alwaysBounceVertical = true
        self.refresher = UIRefreshControl()
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        colExplore!.addSubview(refresher)

        if refresher.isRefreshing {
            refresher.endRefreshing()
        }
        
         txtSeaarch.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.colExplore.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
        self.colExplore.addGestureRecognizer(swipeLeft)
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                pageIndicator.currentPage = 0
                  UIView.transition(with: self.colExplore, duration: 1.0, options: .transitionCrossDissolve, animations: {self.colExplore.reloadData()}, completion: nil)
            case .left:
               pageIndicator.currentPage = 1
                UIView.transition(with: self.colExplore, duration: 1.0, options: .transitionCrossDissolve, animations: {self.colExplore.reloadData()}, completion: nil)
            default:
                break
            }
        }
    }
    
    @objc func loadData(_ sender: AnyObject) {
        if pageIndicator.currentPage == 0{
             if refresher.isRefreshing {
                 refresher.endRefreshing()
             }
            postList = postList.shuffled()
             UIView.transition(with: self.colExplore, duration: 1.0, options: .transitionCrossDissolve, animations: {self.colExplore.reloadData()}, completion: nil)
        }else{
             if refresher.isRefreshing {
                 refresher.endRefreshing()
             }
            postListSecond = postListSecond.shuffled()
             UIView.transition(with: self.colExplore, duration: 1.0, options: .transitionCrossDissolve, animations: {self.colExplore.reloadData()}, completion: nil)
        }
    }
    
  
    
    @objc func textFieldDidChange(textField : UITextField){
        getUserPosts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        pageIndicator.currentPage = 0
        getUserPosts()
        lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "explore", comment: "")
        txtSeaarch.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "search", comment: "")
                 if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                   txtSeaarch.textAlignment = .right
                 }else{
                   txtSeaarch.textAlignment = .left
               }
    }
    

    @IBAction func uploadItem(_ sender: UIButton) {
        showUploadView(sender: self)
    }
    
    @IBAction func searchItem(_ sender: UIButton) {
        showSearchView(sender: self)
    }
    
    func getCategoryList(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
                
        ClassifiedAPIFacade.shared.getCategories { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
        //                if aRes?.json?["success"].intValue == 0{
        //                    showAlert(title: "", subTitle: aRes?.json?["msg"].stringValue ?? "", sender: self)
        //                }else{
                self.allCategories.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Categorys(json: m)
                    self.allCategories.append(booking)
                    }
                    }
                UIView.transition(with: self.colCategory, duration: 1.0, options: .curveEaseIn, animations: {self.colCategory.reloadData()}, completion: nil)
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    func getUserPosts(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
        
        var detail = [String:Any]()
        if selectedCat != 0{
          detail["main_category"] = selectedCat
        }
        detail["keywords"] = txtSeaarch.text
        
        detail["explore_list"] = "1"
        
        ClassifiedAPIFacade.shared.lisPosts(details: detail) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.postList.removeAll()
                self.postListSecond.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Post(json: m)
                    self.postList.append(booking)
                }
                    }
                if let data = aRes?.json?["data_second"].array{
                for m in data{
                    let booking = Post(json: m)
                    self.postListSecond.append(booking)
                }
                    }
                
                UIView.transition(with: self.colExplore, duration: 1.0, options: .transitionCrossDissolve, animations: {self.colExplore.reloadData()}, completion: nil)
                let label = UILabel()
                label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_products", comment: "")
                
                label.textAlignment = .center
                label.textColor = UIColor.darkGray
                label.sizeToFit()
                label.frame = CGRect(x: self.colExplore.frame.width/2, y: self.colExplore.frame.height/2, width: self.colExplore.frame.width, height: 50)
                
                if self.pageIndicator.currentPage ==  0{
                    if self.postList.count == 0{
                        self.colExplore.backgroundView = label
                    }else{
                        self.colExplore.backgroundView = nil
                    }
                }else{
                   if self.postListSecond.count == 0{
                        self.colExplore.backgroundView = label
                    }else{
                        self.colExplore.backgroundView = nil
                    }
                }
               
                
            }else{
                showNetworkError(sender: self)
            }
        }
    }

}

extension ExploreViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 10{
            return allCategories.count+1
        }else{
            if pageIndicator.currentPage == 0{
                return postList.count
            }else{
                return postListSecond.count
            }
           
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 10{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCConst.headerListCellId, for: indexPath) as! ExploreHeaderCell
            if indexPath.row == 0{
                cell.lblItem.text = "All"
            }else{
                cell.lblItem.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[indexPath.item-1].name_ar  : allCategories[indexPath.item-1].name
            }
           //arabic
            if indexPath.item == selectedItem{
                cell.lblItem.textColor = UIColor.black
            }else{
               cell.lblItem.textColor = UIColor.gray
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCConst.listCellId, for: indexPath) as! ExploreListCell
            if pageIndicator.currentPage == 0{
                cell.imgItem.kf.setImage(with: setImage(imageURL: postList[indexPath.row].photo ?? ""))
            }else{
                cell.imgItem.kf.setImage(with: setImage(imageURL: postListSecond[indexPath.row].photo ?? ""))
            }
           
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt
        indexPath: IndexPath) -> CGSize {
        if  collectionView.tag == 20{
            return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.width/3)
        }else{
            if allCategories.count >= 1{
                if indexPath.row == 0{
                    let item = LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: "")
                                   let itemSize = item.size(withAttributes: [
                                                     NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)
                                                     ] )
                                   return itemSize
                }else{
                    let item = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[indexPath.item-1].name_ar  :  allCategories[indexPath.item-1].name
                                   let itemSize = item?.size(withAttributes: [
                                       NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)
                                       ] )
                                   return itemSize!
                }
               
            }else{
                 let item =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: "")
                let itemSize = item.size(withAttributes: [
                                  NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)
                                  ] )
                return itemSize
            }
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 10{
           selectedItem = indexPath.item
            collectionView.reloadData()
            if indexPath.row == 0{
               selectedCat = 0
            }else{
                selectedCat = allCategories[indexPath.item-1].id!
            }
            
            getUserPosts()
        }else{
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.productDetailVc) as! ProductDetailViewController
            if pageIndicator.currentPage == 0{
              vc.selectedProductID = postList[indexPath.row].post_id
            }else{
               vc.selectedProductID = postListSecond[indexPath.row].post_id
            }
             vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }
     
    }
    
    
    
}
