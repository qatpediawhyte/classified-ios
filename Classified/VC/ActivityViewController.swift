//
//  ActivityViewController.swift
//  Classified
//
//  Created by WC_Macmini on 18/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyGif


class ActivityViewController: UIViewController {

  @IBOutlet var imgLoader: UIImageView!
          
        override func viewDidLoad() {
            super.viewDidLoad()
            
           

            
//            do {
//                let gif = try UIImage(gifName: "loader.gif")
//                self.imgLoader.setGifImage(gif)
//            } catch {
//                print(error)
//            }
        }
        

        func loadWith( parent:UIViewController) {
            setParent(parent)

        }
        
        func setParent (_ aParent:UIViewController?) {
           

            if aParent != nil {
                if aParent != self.parent {
                    aParent?.addChild(self)
                    self.didMove(toParent: aParent)
                    self.view.frame = (aParent?.view.frame)!
                    aParent?.view.addSubview(self.view)
                }
            }
        }
        
        func removeFromParentView (_ aParent:UIViewController?)  {

            if aParent?.children.count ?? 0 > 0{
                let viewControllers:[UIViewController] = aParent!.children
                for viewContoller in viewControllers{
                    viewContoller.willMove(toParent: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParent()
                }
            }
        }
}

