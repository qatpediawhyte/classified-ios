//
//  SubCategoryViewController.swift
//  Classified
//
//  Created by WC_Macmini on 19/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class SubCategoryViewController: UIViewController {
    struct VCConst {
           static let collectionCellId = "subCategory_list_cell_id"
    }
       
    var isSubCategory = true
    var subCategoryList = [Categorys]()
    
    var subCategoryId : Int?
    var childCategoryId : Int?
    var heading = ""
    
    @IBOutlet var colCategory: UITableView!
    @IBOutlet var lblHEading: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isSubCategory == false{
                     getChildCategory()
               }else{
                  getSubCategoryList()
               }
        lblHEading.text = heading
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if self.navigationController != nil{
            navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        }
    }
    
    
    func getSubCategoryList(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .overFullScreen
        loadingview?.loadWith(parent: self)
                
        ClassifiedAPIFacade.shared.getSubCategories(categoryID: categoryIdCommon!) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.subCategoryList.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Categorys(json: m)
                    self.subCategoryList.append(booking)
                }
                    }
                UIView.transition(with: self.colCategory, duration: 1.0, options: .curveEaseIn, animations: {self.colCategory.reloadData()}, completion: nil)
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    func getChildCategory(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .overFullScreen
        loadingview?.loadWith(parent: self)
                
        ClassifiedAPIFacade.shared.getChildCategory(subCatId: subCategoryId!) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.subCategoryList.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Categorys(json: m)
                    self.subCategoryList.append(booking)
                }
                    }

                UIView.transition(with: self.colCategory, duration: 1.0, options: .curveEaseIn, animations: {self.colCategory.reloadData()}, completion: nil)

            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    @IBAction func uploadItem(_ sender: UIButton) {
        showUploadView(sender: self)
    }
    
    @IBAction func searchItem(_ sender: UIButton) {
        showSearchView(sender: self)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
       
    }
}

extension SubCategoryViewController : UITableViewDataSource, UITableViewDelegate{
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return subCategoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCConst.collectionCellId, for: indexPath) as! SubCategoryListCell
              
//              let urlString = subCategoryList[indexPath.item].photo?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! ?? ""
//              let imagepath =  URL(string:ClassifiedAPIConfig.BaseUrl.imageBaseServerpath+urlString)
//              cell.imgType.kf.setImage(with: imagepath)
              cell.lblCategory.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? subCategoryList[indexPath.item].name_ar ?? "" : subCategoryList[indexPath.item].name ?? ""
              return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isSubCategory == true{
            subCategoryId = subCategoryList[indexPath.item].id
            if subCategoryList[indexPath.item].child_cat_count ?? 0 >= 1{
                isSubCategory = false
                let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.subCategoryView) as! SubCategoryViewController
                vc.subCategoryId = subCategoryId
                vc.isSubCategory = false
                vc.heading = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? subCategoryList[indexPath.item].name_ar ?? "" : subCategoryList[indexPath.item].name ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
              
            }else{
                let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.homeDetailVc) as! HomeDetailViewController
                vc.subCategoryID = subCategoryId
                vc.header = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? subCategoryList[indexPath.item].name_ar ?? "" : subCategoryList[indexPath.item].name ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else{
            childCategoryId = subCategoryList[indexPath.item].id
            isSubCategory = true
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.homeDetailVc) as! HomeDetailViewController
            vc.childCategoryID = childCategoryId
            vc.subCategoryID = subCategoryId
            vc.header = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? subCategoryList[indexPath.item].name_ar ?? "" : subCategoryList[indexPath.item].name ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
}
extension SubCategoryViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
}
