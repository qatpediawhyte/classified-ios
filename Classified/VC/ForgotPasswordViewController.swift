//
//  ForgotPasswordViewController.swift
//  Classified
//
//  Created by WC_Macmini on 29/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet var btnReset: UIButton!
    @IBOutlet var lblReset: UILabel!
    @IBOutlet var lblEnterEmail: UILabel!
    @IBOutlet var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
    }
    
    func setLanguage(){
        lblReset.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "reset_pass", comment: "")
        lblEnterEmail.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_email", comment: "")
        txtEmail.placeholder  = LocalizationSystem.sharedInstance.localizedStringForKey(key: "email", comment: "")
        btnReset.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "reset_pass", comment: ""), for: .normal)
          if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            lblReset.textAlignment = .right
            txtEmail.textAlignment = .right
          }else{
            lblReset.textAlignment = .left
             txtEmail.textAlignment = .left
        }
    }

    @IBAction func btnBack(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnForgot(_ sender: UIButton) {
       if txtEmail.text == ""{
                  showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_email", comment: "")
, sender: self)
                  return
        }else if txtEmail.text != ""{
            let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailReg)
            guard emailTest.evaluate(with: txtEmail.text) == true else {
                let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "invalid_email", comment: "")
 , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_valid_emal", comment: "")
, preferredStyle: .alert)
                let OkButton = UIAlertAction(title: "OK",style: .cancel) { (alert) -> Void in
            }
                alert.addAction(OkButton)
                self.present(alert, animated: true, completion: nil)
                return
            }
       }
        
    perfomrForgotPass()
        
    }
    
    func perfomrForgotPass(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.forgotPassword(email: txtEmail.text ?? "") { (aResp) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aResp?.statusCode == 200{
                if aResp?.json?["status"].intValue == 200{
                    showAlert(title: "", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                }else{
                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                }
            }else{
               showNetworkError(sender: self)
            }
        }
    }

}
