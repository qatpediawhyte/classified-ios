//
//  LoginViewController.swift
//  Classified
//
//  Created by WC_Macmini on 29/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class LoginViewController: UIViewController {

    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var btnForgot: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var lblLogin: UILabel!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignIn.layer.cornerRadius = 25
        btnSignIn.layer.masksToBounds = true
        btnSignIn.layer.borderWidth = 1
        btnSignIn.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        setLanguage()
    }
    
    func setLanguage(){
        lblLogin.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "login", comment: "")
        txtEmail.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "email", comment: "")
        txtPassword.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "")
        btnSignIn.setTitle(
        LocalizationSystem.sharedInstance.localizedStringForKey(key: "sign_in", comment: ""), for: .normal)
        btnForgot.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "forgot_pass", comment: ""), for: .normal)
        btnSignUp.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sign_up", comment: ""), for:
            .normal)
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            txtEmail.textAlignment = .right
            txtPassword.textAlignment = .right
        }else{
            txtEmail.textAlignment = .left
            txtPassword.textAlignment = .left
        }
    }
    
    @IBAction func btnSignIn(_ sender: UIButton) {
        if txtEmail.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_email", comment: "")
, sender: self)
                   return
        }else if txtPassword.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_password", comment: ""), sender: self)
            return
        }
               
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
               
        var profDic = [String:Any]()
        profDic["email"] = txtEmail.text
        profDic["password"] = txtPassword.text
             
       ClassifiedAPIFacade.shared.login(loginDet:profDic) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
        if aRes?.statusCode == 200{
           if aRes?.json?["status"].intValue == 200{
                saveUserDetails(userDetails: (ClassifiedAPIFacade.shared.session?.currentUser)!)
                self.dismiss(animated: true, completion: nil)
            }else{
               showAlert(title: "Oops!", subTitle: aRes?.json?["message"].stringValue ?? "", sender: self)
            }
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    @IBAction func btnForgot(_ sender: UIButton) {
       let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.forPassVC)
       vc?.modalPresentationStyle = .fullScreen
       self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func btnSignUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.signUpVc) as! SignUpViewController
        vc.loginView = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        let homeView = storyboard?.instantiateViewController(withIdentifier: storyBrdIds.homeView)
        homeView?.modalPresentationStyle = .fullScreen
        self.present(homeView!, animated: true, completion: nil)
    }
}
