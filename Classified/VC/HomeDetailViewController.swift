//
//  HomeDetailViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ImageSlideshow
import ClassifiedWebkit
import AlamofireImage

class HomeDetailViewController: UIViewController,UIScrollViewDelegate {
    struct VCConst {
        static let savedListId = "saved_listCell_id"
        static let categoryCellId = "detail_category_cell_id"
        
    }
    
    @IBOutlet var bannerSlide: ImageSlideshow!
    @IBOutlet var btnViewDetail: ImageSlideshow!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var tblPost:  UITableView!
     @IBOutlet var imglogo: UIImageView!
    
    var header = ""
    var subCategoryID : Int?
    var childCategoryID: Int?
    var bannerList = [Banner]()
    var postList = [Post]()
    var bannerImageList = [AlamofireSource]()
    var selectedProductId = 0
    let itemArray = [["image":"test_8","name":"B&O","count":"5643 Products"],["image":"test_9","name":"Beats","count":"5620 Products"]]
    
    var priceFilter = ""
    var conditionfilter = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeading.text = header
        imglogo.isHidden = true
        getBannerImages()
         getPostList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
           if self.navigationController != nil{
               navigationController?.interactivePopGestureRecognizer?.delegate = self
               navigationController?.interactivePopGestureRecognizer?.isEnabled = true

           }
          
       }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(scrollView.contentOffset.y + scrollView.frame.size.height) <= Int(scrollView.contentSize.height + scrollView.contentInset.bottom) + 210) {
            imglogo.isHidden = true
        }else{
            imglogo.isHidden = false
        }
    }
    
    func setBanner(){
        
        for all in bannerList{
            let imageURl =  AlamofireSource(urlString:ClassifiedAPIConfig.BaseUrl.imageBaseServerpath+all.photo!)
            if imageURl != nil{
                bannerImageList.append(imageURl!)
            }
        }
        
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.black
        pageIndicator.pageIndicatorTintColor = UIColor.darkGray
        bannerSlide.setImageInputs(bannerImageList)
        bannerSlide.slideshowInterval = 5
        bannerSlide.contentScaleMode = .scaleAspectFit
        bannerSlide.layer.cornerRadius = 0
        bannerSlide.layer.masksToBounds = true
        bannerSlide.pageIndicator = pageIndicator
        bannerSlide.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .customBottom(padding: 0) )
        
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeDetailViewController.didTap))
            bannerSlide.addGestureRecognizer(gestureRecognizer)
        bannerSlide.addGestureRecognizer(gestureRecognizer)

    }
    
    @objc func didTap() {
        if self.bannerList[bannerSlide.currentPage].product_id != 0 && self.bannerList[bannerSlide.currentPage].product_id != nil{
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.productDetailVc) as! ProductDetailViewController
                  selectedProductId = bannerList[bannerSlide.currentPage].product_id!
                  vc.selectedProductID = selectedProductId
             vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getBannerImages(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
                
        var bannerDetails = [String:Any]()
        bannerDetails["main_category"] =  categoryIdCommon
        if subCategoryID != nil{
             bannerDetails["sub_category"] =  subCategoryID
        }
        if childCategoryID != nil{
             bannerDetails["child_category"] =  childCategoryID
        }
        
      
        
        
        ClassifiedAPIFacade.shared.getBannerImages(details: bannerDetails) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.bannerList.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Banner(json: m)
                    self.bannerList.append(booking)
                }
                    }
                if self.bannerList.count > 0{
                    self.setBanner()
                }
                
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    func getPostList(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
                
        var bannerDetails = [String:Any]()
        bannerDetails["main_category"] =  categoryIdCommon
        if subCategoryID != nil{
             bannerDetails["sub_category"] =  subCategoryID
        }
        if childCategoryID != nil{
             bannerDetails["child_category"] =  childCategoryID
        }
        
        if priceFilter != ""{
            bannerDetails["price_filter"] =  priceFilter
                         
        }
        if conditionfilter != ""{
            bannerDetails["product_condition"] =  conditionfilter
        }

        
        ClassifiedAPIFacade.shared.lisPosts(details: bannerDetails) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.postList.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Post(json: m)
                    self.postList.append(booking)
                }
                    }
                
                UIView.transition(with: self.tblPost, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblPost.reloadData()}, completion: nil)
                
                let label = UILabel()
                label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_products", comment: "")
                               
                label.textAlignment = .center
                label.textColor = UIColor.darkGray
                label.sizeToFit()
                label.frame = CGRect(x: self.tblPost.frame.width/2, y: self.tblPost.frame.height/2, width: self.tblPost.frame.width, height: 50)
                               
                if self.postList.count == 0{
                    self.tblPost.backgroundView = label
                }else{
                    self.tblPost.backgroundView = nil
                }
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
   
    
    @IBAction func btnBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadItem(_ sender: UIButton) {
        showUploadView(sender: self)
    }
    
    @IBAction func searchItem(_ sender: UIButton) {
        showSearchView(sender: self)
    }
    
    @IBAction func btnFilter(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.filterVC) as! FilterViewController
        vc.prodyctListView = self
        self.present(vc, animated: true, completion: nil)
    }

}
extension HomeDetailViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postList.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: VCConst.savedListId,  for: indexPath) as! SavedListCell
        cell.selectionStyle = .none

        if postList[indexPath.row].wish_list == 0{
            cell.imgWishList.image = UIImage(named: "wishList_black")
        }else{
            cell.imgWishList.image = UIImage(named: "wishListRed")
        }
        
        cell.lblName.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? postList[indexPath.row].name_ar : postList[indexPath.row].name ?? "" //arabic
        cell.lblTime.text = postList[indexPath.row].time_agos ?? "" //arabic
        cell.lblSellerName.text = postList[indexPath.row].user_name ?? ""
        if postList[indexPath.row].product_condition == 0{
             cell.lblUsedCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
        }else{
             cell.lblUsedCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: "")
        }
       
        let urlString = postList[indexPath.row].photo?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! ?? ""
              let imagepath =  URL(string:ClassifiedAPIConfig.BaseUrl.imageBaseServerpath+urlString)
        cell.imgItem.kf.setImage(with: imagepath)
        
        var previousPrice = ""
        var price = ""
        
        postList[indexPath.row].previous_price =  nil
        
        if postList[indexPath.row].previous_price != nil && postList[indexPath.row].previous_price != "0" &&  postList[indexPath.row].previous_price != ""{
            previousPrice = postList[indexPath.row].previous_price!  + "QAR "
            
            if postList[indexPath.row].price != nil{
               price = postList[indexPath.row].price!  + "QAR "
            }
         
            let attributedText = NSMutableAttributedString(string:previousPrice , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12 , weight: .light)])
                  attributedText.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedText.length))
                  attributedText.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.darkGray, range: NSMakeRange(0, attributedText.length))

                  
                       attributedText.append(NSAttributedString(string: price, attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16 , weight: .medium)]))
                  cell.lblPrice.attributedText = attributedText
        }else{
            if postList[indexPath.row].price != nil{
               price = postList[indexPath.row].price!  + "QAR "
            }
            
            cell.lblPrice.text = "\(price)"
        }
        
        
        
        
      
        
        cell.btnCall.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "call", comment: ""), for: .normal)
        
        cell.setCallAction { (aCell) in
            if let url = URL(string: "tel://\(self.postList[indexPath.row].phone_number ?? "")"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.setcommentAction { (aCell) in
            let vc = self.storyboard?.instantiateViewController(identifier:storyBrdIds.reviewVC ) as! ReviewViewController
            vc.postId = self.postList[indexPath.row].post_id!
            vc.postedUserId = self.postList[indexPath.row].user_id!
            vc.isFromList = true
             vc.modalPresentationStyle = .fullScreen
            vc.postName = (LocalizationSystem.sharedInstance.getLanguage() == "ar" ? self.postList[indexPath.row].name_ar :  self.postList[indexPath.row].name) ?? ""
            if self.navigationController == nil{
              self.present(vc, animated: true, completion: nil)
            }else{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.productDetailVc) as! ProductDetailViewController
        selectedProductId = postList[indexPath.row].post_id!
        vc.selectedProductID = selectedProductId
         vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension HomeDetailViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCConst.categoryCellId, for: indexPath) as! DetailCategoryCell
        cell.lblName.text = itemArray[indexPath.item]["name"]
        cell.lblCount.text = itemArray[indexPath.item]["count"]
        cell.imgItem.image = UIImage(named: itemArray[indexPath.item]["image"] ?? "")
        
        
            return cell
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt
        indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 70)
    }
}

extension HomeDetailViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
}
