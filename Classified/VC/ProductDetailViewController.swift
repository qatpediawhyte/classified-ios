//
//  ProductDetailViewController.swift
//  Classified
//
//  Created by WC_Macmini on 01/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ImageSlideshow
import ClassifiedWebkit
import HCSStarRatingView

class ProductDetailViewController: UIViewController {

    struct VCConst {
        static let productBasicDetailsId = "productBasic_cell_id"
        static let productDetailsId = "productDetailos_cell_id"
        static let productSellerDetailsId = "product_seller_cell_id"
        static let productReviewDetailsId = "product_reviewDetails_cell_id"
        static let reviewListId = "review_list_cell_id"
    }
    
    @IBOutlet var tblPost:  UITableView!
    @IBOutlet var imgSeller:  UIImageView!
    @IBOutlet var imgSellerAr:  UIImageView!
    @IBOutlet var lblSeller:  UILabel!
    @IBOutlet var viewPromot: UIView!
    @IBOutlet var rating: HCSStarRatingView!
    @IBOutlet var ratingView: UIView!
    @IBOutlet var viewAr: UIView!
    
    var isLoaded = false
    var isAdded = false
    var selectedProductID : Int?
    var isFromSeller = false
    var isFromProfile = false
    
    var productName = ""
    var productRating = ""
    var productNameAr = ""
    var viewCount = 0
    var image = ""
    var daysCount = ""
    var sellerName = ""
    var price = 0
    var phoneNo = ""
    var specification = ""
    var specificationAr = ""
    var details = ""
    var detailsAr = ""
    var postedUserName = ""
    var postedMemberSince = ""
    var postedMemberId = 0
    var postedMembeRating = ""
    var isNew = 0
    var isWishList = 0
    var previousPrice = 0
    var sellerImage = ""
    
    var brand = ""
    var brandAr = ""
    var model = ""
    var modelAr = ""
    var location = ""
    var locationAr = ""
    var reviewList = [Review]()
    var height = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("ViewPromote", owner: self, options: nil)
        Bundle.main.loadNibNamed("RatingView", owner: self, options: nil)
        
        getProductDetail()
         if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            viewAr.isHidden = false
         }else{
            viewAr.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.navigationController != nil{
            navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        }
    }

    private func setAttributedTexts(name:String,text:String) -> NSMutableAttributedString{
          if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            let attributedText = NSMutableAttributedString(string:text , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)])
                       attributedText.append(NSAttributedString(string: "\(name)" , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12,weight:  .medium)]))
                                 return attributedText
          }else{
            let attributedText = NSMutableAttributedString(string:"\(name)" , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12 , weight: .medium)])
                                attributedText.append(NSAttributedString(string: " \(text) " , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]))
                                return attributedText
        }
          
       }
    
    func addRating(){
           let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                  loadingview?.modalPresentationStyle = .fullScreen
                  loadingview?.loadWith(parent: self)
                  
           let value:Int = Int(rating?.value ?? 0)

           
        ClassifiedAPIFacade.shared.addProductRating(rating: value, productId: selectedProductID!) { (aResp) in
                      DispatchQueue.main.async {
                          loadingview?.removeFromParentView(self)
                      }
                      if aResp?.statusCode == 200{
                          if aResp?.json?["status"].intValue == 200{
                              let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue ?? "", preferredStyle: .alert)
                                            let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
                                               self.ratingView.removeFromSuperview()
                                            }
                               alert.addAction(OkButton)
                               self.present(alert, animated: true, completion: nil)
                          }else{
                              showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                          }
                      }else{
                        showNetworkError(sender: self)
                      }
                  }
       }
    
    func savePost(postID:Int){
           let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
           loadingview?.modalPresentationStyle = .fullScreen
           loadingview?.loadWith(parent: self)
           
        if self.isWishList == 0{
            self.isWishList = 1
        }else{
            self.isWishList = 0
        }
        
        ClassifiedAPIFacade.shared.savePost(prodID: postID, save: isWishList){(aResp) in
               DispatchQueue.main.async {
                   loadingview?.removeFromParentView(self)
               }
            
               if aResp?.statusCode == 200{
                   if aResp?.json?["status"].intValue == 200{
                    self.tblPost.reloadData()
                   }else{
                       showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                   }
               }else{
                  showNetworkError(sender: self)
               }
           }
       }
    
    func getProductDetail(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
              loadingview?.modalPresentationStyle = .fullScreen
              loadingview?.loadWith(parent: self)
        
        ClassifiedAPIFacade.shared.getProductDetails(prodID: selectedProductID!) { (aRes) in
                  DispatchQueue.main.async {
                      loadingview?.removeFromParentView(self)
                  }
                  if aRes?.statusCode == 200{
                      if let data = aRes?.json?["data"].dictionary{
                        self.productName = data["name"]?.stringValue ?? ""
                        self.productNameAr = data["name_ar"]?.stringValue ?? ""
                        self.viewCount = data["views"]?.intValue ?? 0
                        self.daysCount = data["time_ago"]?.stringValue ?? ""
                        self.sellerName = data["posted_user_name"]?.stringValue ?? ""
                        self.price = data["price"]?.intValue ?? 0
                        self.phoneNo =  data["posted_phone_number"]?.stringValue ?? ""
                        self.specification =  data["specification"]?.stringValue ?? ""
                        self.specificationAr = data["specification_ar"]?.stringValue ?? ""
                        self.details = data["details"]?.stringValue ?? ""
                        self.detailsAr = data["details"]?.stringValue ?? ""
                        self.postedMemberSince = data["posted_mamber_since"]?.stringValue ?? ""
                        self.isNew = Int(data["product_condition"]?.stringValue ?? "0") ?? 0
                        self.isWishList = data["wish_list"]?.intValue ?? 0
                        self.image = data["photo"]?.stringValue ?? ""
                        self.previousPrice = data["previous_price"]?.intValue ?? 0
                        self.brand = data["brand"]?.stringValue ?? ""
                        self.brandAr = data["brand_ar"]?.stringValue ?? ""
                        self.model = data["model"]?.stringValue ?? ""
                        self.modelAr = data["model_ar"]?.stringValue ?? ""
                        self.location = data["location"]?.stringValue ?? ""
                        self.locationAr = data["location_ar"]?.stringValue ?? ""
                        self.sellerImage = data["posted_mamber_image"]?.stringValue ?? ""
                        self.postedMemberId = data["posted_user_id"]?.intValue ?? 0
                        self.postedMembeRating = "\(data["rating"]?.intValue ?? 0)"
                        self.productRating = "\(data["product_rating"]?.intValue ?? 0)"
                        
                        let reviewArray =  data["reviews_list"]?.array
                        self.reviewList.removeAll()
                        if reviewArray != nil{
                            for m in reviewArray!{
                                let booking = Review(json: m)
                                self.reviewList.append(booking)
                            }
                        }
                        
                        if self.sellerImage != ""{
                            self.imgSeller.kf.setImage(with: setImage(imageURL: self.sellerImage))
                        }else{
                           self.imgSeller.image = UIImage(named: "profile_thumb")
                        }
                        
                        if self.sellerImage != ""{
                            self.imgSellerAr.kf.setImage(with: setImage(imageURL: self.sellerImage))
                        }else{
                            self.imgSellerAr.image = UIImage(named: "profile_thumb")
                        }
                        
                        
                        //self.lblSeller.text = self.sellerName
                    }
                    self.tblPost.reloadData()
                  }else{
                      showNetworkError(sender: self)
                  }
              }
    }
    
    func deletePost(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                     loadingview?.modalPresentationStyle = .fullScreen
                     loadingview?.loadWith(parent: self)

               ClassifiedAPIFacade.shared.deletePost(postID: selectedProductID!){ (aResp) in
                         DispatchQueue.main.async {
                             loadingview?.removeFromParentView(self)
                         }
                         if aResp?.statusCode == 200{
                             if aResp?.json?["status"].intValue == 200{
                                let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                  let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                              ,style: .cancel) { (alert) -> Void in
                                                                if self.navigationController != nil{
                                                                    self.navigationController?.popViewController(animated: true)
                                                                }else{
                                                                    self.dismiss(animated: true, completion: nil)
                                                                }
                                                                                  }
                                                                                  alert.addAction(OkButton)
                                                                                  self.present(alert, animated: true, completion: nil)
                             }else{
                               let alert = UIAlertController(title: "Oops!" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                                         let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                                                     ,style: .cancel) { (alert) -> Void in
                                                                                                         }
                                                                                                         alert.addAction(OkButton)
                                                                                                         self.present(alert, animated: true, completion: nil)
                               
                             }
                         }else{
                           showNetworkError(sender: self)
                         }
                     }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        if self.navigationController == nil{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnshowSellerDetails(_ sender: UIButton) {
           
        if isFromSeller == false && isFromProfile == false{
            let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.profileVC) as? ProfileViewController
            vc?.isFromSettings = true
            vc?.isSeller = true
            vc?.sellerNAme = self.sellerName
            vc?.sellerMEmber = self.postedMemberSince
            vc?.sellerImage = self.sellerImage
            vc?.sellerID = self.postedMemberId
            vc?.sellerRating = self.postedMembeRating
            vc?.modalPresentationStyle = .fullScreen
            if self.navigationController == nil{
                self.present(vc!, animated: true, completion: nil)
            }else{
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        }
    }
    
    @IBAction func btnMakeBannerAd(_ sender: UIButton) {
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
              loadingview?.modalPresentationStyle = .fullScreen
              loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.makeBannerAd(postID: selectedProductID! ){ (aResp) in
                  DispatchQueue.main.async {
                      loadingview?.removeFromParentView(self)
                  }
                  if aResp?.statusCode == 200{
                      if aResp?.json?["status"].intValue == 200{
                         let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                           let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                       ,style: .cancel) { (alert) -> Void in
                                                                                self.viewPromot.removeFromSuperview()
                                                                           }
                                                                           alert.addAction(OkButton)
                                                                           self.present(alert, animated: true, completion: nil)
                      }else if aResp?.json?["status"].intValue == 405{
                            let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                                                         let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                                                                     ,style: .cancel) { (alert) -> Void in
                                                                                                        self.viewPromot.removeFromSuperview()
                                                                                                         let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.subscriptionVC) as? SubscriptionViewController
                                                                                                        vc?.modalPresentationStyle = .fullScreen
                                                                                                        self.present(vc!, animated: true, completion: nil)}
                                                                                                                         alert.addAction(OkButton)
                                                                                                                         self.present(alert, animated: true, completion: nil)
                                               
                      }else{
                        let alert = UIAlertController(title: "Oops!" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                                  let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                                              ,style: .cancel) { (alert) -> Void in
                                                                                                       self.viewPromot.removeFromSuperview()
                                                                                                  }
                                                                                                  alert.addAction(OkButton)
                                                                                                  self.present(alert, animated: true, completion: nil)
                        
                      }
                  }else{
                    showNetworkError(sender: self)
                  }
              }
    }
    
    @IBAction func btnMakeFeature(_ sender: UIButton) {
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                     loadingview?.modalPresentationStyle = .fullScreen
                     loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.makeFeatureAd(postID: selectedProductID!){ (aResp) in
                         DispatchQueue.main.async {
                             loadingview?.removeFromParentView(self)
                         }
                         if aResp?.statusCode == 200{
                             if aResp?.json?["status"].intValue == 200{
                                  let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                    let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                ,style: .cancel) { (alert) -> Void in
                                                         self.viewPromot.removeFromSuperview()
                                                    }
                                                    alert.addAction(OkButton)
                                                    self.present(alert, animated: true, completion: nil)
                                
                               
                             }else if aResp?.json?["status"].intValue == 405{
                                   let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                                                                let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                                                                            ,style: .cancel) { (alert) -> Void in
                                                                                                                self.viewPromot.removeFromSuperview()
                                                                                                                let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.subscriptionVC) as? SubscriptionViewController
                                                                                                          vc?.modalPresentationStyle = .fullScreen
                                                                                                                self.present(vc!, animated: true, completion: nil)}
                                                                                                                                alert.addAction(OkButton)
                                                                                                                                self.present(alert, animated: true, completion: nil)
                                                      
                             }
                             else{
                                   let alert = UIAlertController(title: "Oops!" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                                                                let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                                                                            ,style: .cancel) { (alert) -> Void in
                                                                                                                                     self.viewPromot.removeFromSuperview()
                                                                                                                                }
                                                                                                                                alert.addAction(OkButton)
                                                                                                                                self.present(alert, animated: true, completion: nil)
                                                      
                             }
                         }else{
                           showNetworkError(sender: self)
                         }
                     }
    }
    
    @IBAction func btnCancelPromote(_ sender: UIButton) {
        viewPromot.removeFromSuperview()
    }
    
    @IBAction func btnSubmnitRating(_ sender: UIButton) {
        if sender.tag == 10{
          addRating()
        }else{
          ratingView.removeFromSuperview()
        }
    }
}

extension ProductDetailViewController : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 4
        }else{
            return reviewList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                        let cell  = tableView.dequeueReusableCell(withIdentifier: VCConst.productBasicDetailsId, for: indexPath) as! ProductBasicCell
            //                let pageIndicator = UIPageControl()
            //                pageIndicator.currentPageIndicatorTintColor = UIColor.black
            //                pageIndicator.pageIndicatorTintColor = UIColor.darkGray
            //                cell.productSlideshow.setImageInputs([ImageSource(image: UIImage(named: "test_14")!),ImageSource(image: UIImage(named: "test_14")!),ImageSource(image: UIImage(named: "test_14")!)])
            //                cell.productSlideshow.slideshowInterval = 2
            //                cell.productSlideshow.contentScaleMode = .scaleAspectFill
            //                cell.productSlideshow.layer.cornerRadius = 0
            //                cell.productSlideshow.layer.masksToBounds = true
            //                cell.productSlideshow.pageIndicator = pageIndicator
            //                cell.productSlideshow.bringSubviewToFront(cell.btnWish)
            //                cell.productSlideshow.bringSubviewToFront(cell.lblIsNew)
            //               / cell.productSlideshow.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .bottom)
              
                
                if  let n = NumberFormatter().number(from: productRating) {
                    cell.rating.value = CGFloat(truncating: n)
                    cell.lblRating.text = "\(productRating)/5"
                }
                else{
                    cell.rating.value = 0
                    cell.lblRating.text = "0/5"
                }
               
                cell.lblSpecHading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "spec", comment: "")
//                cell.imgProduct.kf.setImage(with: setImage(imageURL: image))
//                cell.imgProduct.contentMode = .scaleAspectFill
                cell.imgProduct.contentMode = .scaleAspectFill
                cell.imgProduct.kf.indicatorType = .activity
                cell.imgProduct.kf.setImage(
                    with: setImage(imageURL: image),
                    placeholder: UIImage(named: "placeholderImage"),
                    options: [
                    ])
                {
                    result in
                    switch result {
                    case .success(let value):
                        
                        
                       // cell.imgProduct.image = resizeImage(image: cell.imgProduct.image!, targetSize: CGSize(width: cell.imgProduct.image?.size.width ?? 0, height: cell.imgProduct.image?.size.height ?? 0))
                        DispatchQueue.main.async {
                            if self.isLoaded == false{
                                self.isLoaded = true
//                                cell.setNeedsLayout()
//                                cell.layoutIfNeeded()
//                                cell.updateConstraints()
//                                cell.setNeedsUpdateConstraints()
                                tableView.reloadData()
//                                tableView.beginUpdates()
//                                tableView.endUpdates()
                            }
                            
                        }
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                }
                
                cell.lblProductName.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? productNameAr : productName
               
                
                if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                     cell.lblViewCount.text = "\(viewCount) الآراء"
                }else{
                   cell.lblViewCount.text = "\(viewCount) Views"
                }
                       
                        cell.lblDaysCount.text = daysCount
                        cell.lblSellerName.text = sellerName
                        if isNew == 0{
                            cell.lblIsNew.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                        }else{
                            cell.lblIsNew.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: "")
                        }
                        
                        if isWishList == 1{
                            cell.btnWish.setImage(UIImage(named: "wishRed_big"), for: .normal)
                        }else{
                            cell.btnWish.setImage(UIImage(named: "wishBlack_big"), for: .normal)
                        }
  
                self.previousPrice = 0
                
                if self.previousPrice != 0{

                    let attributedText = NSMutableAttributedString(string:"\(previousPrice) QAR " , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12 , weight: .light)])
                    attributedText.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedText.length))
                    attributedText.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.darkGray, range: NSMakeRange(0, attributedText.length))

                    
                    attributedText.append(NSAttributedString(string: "\(price) QAR", attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16 , weight: .medium)]))
                    cell.lblPrice.attributedText = attributedText
                }else{
                    cell.lblPrice.text = "\(price) QAR"
                }
                     
                if isFromProfile == true{
                    cell.btnCall.isHidden = true
                    cell.btnComment.isHidden = true
                     cell.profileStack.isHidden = false
                }else{
                    cell.profileStack.isHidden = true
                    cell.btnCall.isHidden = false
                    cell.btnComment.isHidden = false
                   cell.btnCall.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "call", comment: ""), for: .normal)
                    cell.btnComment.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment", comment: ""), for: .normal)
                }
                    
                
                cell.commentAction { (aCell) in
                                               
                                                
                        let vc = self.storyboard?.instantiateViewController(identifier:storyBrdIds.reviewVC ) as! ReviewViewController
                        vc.postId = self.selectedProductID!
                        vc.postedUserId = self.postedMemberId
                        vc.productDetaiView = self
                        vc.postName = (LocalizationSystem.sharedInstance.getLanguage() == "ar" ? self.productNameAr :  self.productName)
                     vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)

                }
                        cell.setCallAction { (aCell) in
                          
                            
                            if let url = URL(string: "tel://\(self.phoneNo)"), UIApplication.shared.canOpenURL(url) {
                                     if #available(iOS 10, *) {
                                         UIApplication.shared.open(url)
                                     } else {
                                         UIApplication.shared.openURL(url)
                                     }
                                 }
                             }
                        
                        cell.wishListAction { (aCell) in
                            self.savePost(postID: self.selectedProductID!)
                        }
                
                    cell.editAction { (aCell) in
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(identifier: storyBrdIds.uploadVC) as! UploadViewController
                        vc.selectedPost = self.selectedProductID
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                
                cell.prmoteAction{ (aCell) in
                    self.viewPromot.frame = self.view.bounds
                              self.view.addSubview(self.viewPromot)
                               self.view.bringSubviewToFront(self.viewPromot)
//                              self.viewPromot.transform = self.viewPromot.transform.scaledBy(x: 0.001, y: 0.001)
//                               UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
//                                   self.viewPromot.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
//                              }, completion: nil)
                }
                
                cell.deleteAction{ (aCell) in
                        let alert = UIAlertController(title: "" , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "delete_post", comment: "")
, preferredStyle: .alert)
                                      let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                      ,style: .default) { (alert) -> Void in
                                        
                                        self.deletePost()
                                      }
                                let cancel = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "")
                                                         ,style: .cancel) { (alert) -> Void in
                                                           
                                                         }
                                      alert.addAction(OkButton)
                                    alert.addAction(cancel)
                                      self.present(alert, animated: true, completion: nil)
                }
                
                cell.ratingAction { (aCell) in
                    self.ratingView.frame = self.view.bounds
                    self.view.addSubview(self.ratingView)
                    self.view.bringSubviewToFront(self.ratingView)
                    self.rating.value = 0
                }
                        
                        return cell
                    }else if indexPath.row == 1{
                        let cell  = tableView.dequeueReusableCell(withIdentifier: VCConst.productDetailsId, for: indexPath) as! PrductDetailsCell
                cell.lblBrand.text =  LocalizationSystem.sharedInstance.getLanguage() == "ar" ? "العلامة التجارية: \(brandAr)" : "Brand: \(brand)"

                cell.lblModel.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? "نموذج: \(modelAr)" :  "Model: \(model)"
                cell.lblSpecification.text =  LocalizationSystem.sharedInstance.getLanguage() == "ar" ? "تخصيص: \(specificationAr)" :  "Specification: \(specification)".htmlToString
                cell.lblLocation.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? "موقعك: \(locationAr)" : "Location: \(location)"
                        return cell
                    }else if indexPath.row == 2{
                        let cell  = tableView.dequeueReusableCell(withIdentifier: VCConst.productSellerDetailsId, for: indexPath) as! PorudtSellerDetailsCell
                cell.lblDetailsHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
                if isFromSeller == true{
                    cell.btnViewProfile.isHidden = true
                }else{
                    cell.btnViewProfile.isHidden = false
                     cell.btnViewProfile.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_profile", comment: ""), for: .normal)
                }
              
                cell.lnlMember.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "member", comment: "")
                cell.lblReviewHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: "")
                cell.btnWriteReview.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "write_review", comment: ""), for: .normal)
                cell.lblDetails.text =  LocalizationSystem.sharedInstance.getLanguage() == "ar" ? detailsAr : details.htmlToString
                        cell.lblSellerName.text = sellerName
                        cell.lblMemberSince.text = postedMemberSince
                        cell.imgSellerName.kf.setImage(with: setImage(imageURL: sellerImage))
                        cell.reviewTappedAction { (aCell) in
                            
                            
//                            let vc = self.storyboard?.instantiateViewController(identifier:storyBrdIds.reviewVC ) as! ReviewViewController
//                            vc.postId = self.self.selectedProductID!
//                            vc.productDetaiView = self
//                            self.present(vc, animated: true, completion: nil)
                        }
                cell.lblDellserDetailsHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "seller_details", comment: "")

                        cell.viewProfileTappedActions { (aCell) in
                           
                            let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.profileVC) as? ProfileViewController
                            vc?.isFromSettings = true
                            vc?.isSeller = true
                            vc?.sellerNAme = self.sellerName
                            vc?.sellerMEmber = self.postedMemberSince
                            vc?.sellerImage = self.sellerImage
                            vc?.sellerID = self.postedMemberId
                            vc?.modalPresentationStyle = .fullScreen
                            if self.navigationController == nil{
                                self.present(vc!, animated: true, completion: nil)
                            }else{
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                           
                        }
                        return cell
                    }else{
                        var cell  = tableView.dequeueReusableCell(withIdentifier: VCConst.productReviewDetailsId, for: indexPath) as! ProductReviewDetailsCell
                        cell = ProductReviewDetailsCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: VCConst.productReviewDetailsId)
                        
                        //cell.dataArr = [["image":"","text":""]]
                        return cell
                    }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: VCConst.reviewListId, for: indexPath) as! ReviewListCell
                   cell.selectionStyle = .none
                   cell.txtReview.attributedText = setAttributedTexts(name: reviewList[indexPath.row].user_name ?? "", text: reviewList[indexPath.row].review ?? "")
                   cell.txtReview.sizeToFit()
            if reviewList[indexPath.row].user_photo != ""{
                cell.imgReview.kf.setImage(with: setImage(imageURL: reviewList[indexPath.row].user_photo ?? ""))
            }else{
               cell.imgReview.image = UIImage(named: "profile_thumb")
            }
                   
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 0{
                return UITableView.automaticDimension
            }else if indexPath.row == 1{
                return UITableView.automaticDimension
            }else if indexPath.row == 2{
                return UITableView.automaticDimension
            }else{
                return 0
            }
        }else{
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
}

extension ProductDetailViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
}
