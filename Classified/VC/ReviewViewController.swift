//
//  ReviewViewController.swift
//  Classified
//
//  Created by WC_Macmini on 01/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit

class ReviewViewController: UIViewController,UITextViewDelegate {

    struct VCConst {
        static let reviewListId = "review_list_cell_id"
    }
    
    @IBOutlet var txtReview: UITextField!
    @IBOutlet var btnSend: UIButton!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var tblReview: UITableView!
    @IBOutlet var commentTextView: UIView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var reportView: UIView!
    
    var postId = 0
    var postedUserId = 0
    var postName = ""
    var productDetaiView = ProductDetailViewController()
    var isFromList = false
    var reviewList = [Review]()
    var comId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //view
        Bundle.main.loadNibNamed("ReportView", owner: self, options: nil)
        commentTextView.layer.cornerRadius = commentTextView.frame.height / 2
        commentTextView.layer.borderWidth = 0.5
        commentTextView.layer.borderColor = UIColor.lightGray.cgColor
        commentTextView.backgroundColor = .white
        lblHeading.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "write_review", comment: "")
        btnSend.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "post", comment: ""), for: .normal)
        self.txtReview.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_comment", comment: "")
        if getUserDetails()?["photo"] as? String != "" && getUserDetails()?["photo"] as? String != nil{
            imgUser.kf.setImage(with: setImage(imageURL: getUserDetails()?["photo"] as? String ?? ""))
        }else{
          self.imgUser.image = UIImage(named: "profile_thumb")
        }
        
        getReviewList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.navigationController != nil{
            navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        }
    }
    
    func deleteComment(commentId:Int){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.deleteComment(commentID: commentId) { (aResp) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aResp?.statusCode == 200{
                if aResp?.json?["status"].intValue == 200{
                    //self.getNotificationList()
                }else{
                    showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                }
            }else{
              showNetworkError(sender: self)
            }
        }
    }
    
    func reportcomment(commentId:Int,text:String){
           let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
           loadingview?.modalPresentationStyle = .fullScreen
           loadingview?.loadWith(parent: self)
  
        var detils = [String:Any]()
            detils["comment_id"] = commentId
            detils["comment"] = text
           ClassifiedAPIFacade.shared.reportcomment(details: detils) { (aResp) in
               DispatchQueue.main.async {
                   loadingview?.removeFromParentView(self)
                    self.reportView.removeFromSuperview()
               }
               if aResp?.statusCode == 200{
                   if aResp?.json?["status"].intValue == 200{
                        showAlert(title: "", subTitle: "send_feedback", sender: self)
                   }else{
                        showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                   }
               }else{
                 showNetworkError(sender: self)
               }
           }
       }
    
    @IBAction func btnBack(_ sender: UIButton) {
        if self.navigationController == nil{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func setAttributedTexts(name:String,text:String) -> NSMutableAttributedString{
             if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
               let attributedText = NSMutableAttributedString(string:text , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)])
                          attributedText.append(NSAttributedString(string: "\(name)" , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12,weight:  .medium)]))
                                    return attributedText
             }else{
               let attributedText = NSMutableAttributedString(string:"\(name)" , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12 , weight: .medium)])
                                   attributedText.append(NSAttributedString(string: " \(text) " , attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]))
                                   return attributedText
           }
             
          }
    
    @IBAction func btnSend(_ sender: UIButton) {
       sendReview()
    }
    
    @IBAction func reportSpam(_ sender: UIButton) {
           if sender.tag == 30{
              reportView.removeFromSuperview()
           }else{
            reportcomment(commentId: comId, text: sender.tag == 10 ? "spam" : "inappropriate")
           }
           
       }
    
    func getReviewList(){
           let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                 loadingview?.modalPresentationStyle = .fullScreen
                 loadingview?.loadWith(parent: self)
           
        ClassifiedAPIFacade.shared.getProductDetails(prodID: postId) { (aRes) in
                     DispatchQueue.main.async {
                         loadingview?.removeFromParentView(self)
                     }
                     if aRes?.statusCode == 200{
                         if let data = aRes?.json?["data"].dictionary{
                           self.reviewList.removeAll()
                            let reviewArray =  data["reviews_list"]?.array
                           if reviewArray != nil{
                               for m in reviewArray!{
                                   let booking = Review(json: m)
                                   self.reviewList.append(booking)
                               }
                           }
                       }
                       self.tblReview.reloadData()
                     }else{
                         showNetworkError(sender: self)
                     }
                 }
       }
    
    func sendReview(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                 loadingview?.modalPresentationStyle = .fullScreen
                 loadingview?.loadWith(parent: self)
                 
            var detail = [String:Any]()
            detail["review"] = txtReview.text
            detail["post_id"] = postId
            
                 ClassifiedAPIFacade.shared.writeReview(details:detail){(aResp) in
                     DispatchQueue.main.async {
                         loadingview?.removeFromParentView(self)
                     }
                     if aResp?.statusCode == 200{
                         if aResp?.json?["status"].intValue == 200{
//                            let alert = UIAlertController(title: "" , message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "review_posted", comment: "")
//, preferredStyle: .alert)
//                            let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
//,style: .cancel) { (alert) -> Void in
//                                if self.navigationController == nil{
//                                    if self.isFromList == false{
//                                        self.productDetaiView.getProductDetail()
//                                        self.dismiss(animated: true, completion: nil)
//                                    }else{
//                                       self.dismiss(animated: true, completion: nil)
//                                    }
//
//                                }else{
//                                    if self.isFromList == false{
//                                        self.productDetaiView.getProductDetail()
//                                        self.navigationController?.popViewController(animated: true)
//                                    }else{
//                                      self.navigationController?.popViewController(animated: true)
//                                    }
//
//                                }
//                            }
//                                alert.addAction(OkButton)
//                                self.present(alert, animated: true, completion: nil)
                            
                            self.getReviewList()
                            self.txtReview.text = ""
                         }else{
                             showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                         }
                     }else{
                        showNetworkError(sender: self)
                     }
                 }
    }
}


extension ReviewViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isEqual(navigationController?.interactivePopGestureRecognizer) {
            navigationController?.popViewController(animated: true)
        }
        return false
    }
}

extension ReviewViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCConst.reviewListId, for: indexPath) as! ReviewListCell
        cell.selectionStyle = .none
        cell.txtReview.attributedText = setAttributedTexts(name: reviewList[indexPath.row].user_name ?? "", text: reviewList[indexPath.row].review ?? "")
        cell.txtReview.sizeToFit()
        if reviewList[indexPath.row].user_photo != ""{
            cell.imgReview.kf.setImage(with: setImage(imageURL: reviewList[indexPath.row].user_photo ?? ""))
        }else{
            cell.imgReview.image = UIImage(named: "profile_thumb")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
              -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                self.deleteComment(commentId: self.reviewList[indexPath.row].id!)
                self.reviewList.remove(at: indexPath.row)
                UIView.transition(with: self.tblReview, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblReview.reloadData()}, completion: nil)
            }
            deleteAction.image = UIImage(systemName: "trash")
            deleteAction.backgroundColor = .systemRed
            let reportAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                self.comId = self.reviewList[indexPath.row].id!
                self.reportView.frame = self.view.bounds
                self.view.addSubview(self.reportView)
                self.view.bringSubviewToFront(self.reportView)
                
            }
            reportAction.image = UIImage(systemName: "flag.fill")
            reportAction.backgroundColor = .systemBlue

                
            var  configuration = UISwipeActionsConfiguration()
            let userID = getUserDetails()?["id"] as? Int
            if reviewList[indexPath.row].user_id == userID || postedUserId == userID{
               configuration = UISwipeActionsConfiguration(actions: [deleteAction,reportAction])
            }else{
               configuration = UISwipeActionsConfiguration(actions: [reportAction])
            }
              
              return configuration
      }
}
