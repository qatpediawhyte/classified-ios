//
//  UploadViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import ClassifiedWebkit
import FTPopOverMenu_Swift
import MapKit
import YPImagePicker

class UploadViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet var picker: UIPickerView!
    @IBOutlet var pickerView: UIView!
    @IBOutlet var viewPromot: UIView!
    @IBOutlet var btnUpload: UIButton!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var txtProductName: UITextField!
    @IBOutlet var txtProductNameArabic: UITextField!
    @IBOutlet var btnNew: UIButton!
    @IBOutlet var btnUsed: UIButton!
    @IBOutlet var txtCategory: UITextField!
    @IBOutlet var txtSubCategory: UITextField!
     @IBOutlet var txtProductCondition: UITextField!
    @IBOutlet var viewSubCategory: UIView!
    @IBOutlet var txtChildCategory: UITextField!
    @IBOutlet var viewChildCategory: UIView!
    @IBOutlet var txtBrand: UITextField!
    @IBOutlet var txtBrandArabic: UITextField!
    @IBOutlet var txtModel: UITextField!
    @IBOutlet var txtModelArabic: UITextField!
    @IBOutlet var txtSpecification: UITextView!
    @IBOutlet var txtSpecificationArabic: UITextView!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet var txtLocationArabic: UITextField!
    @IBOutlet var txtDescription: UITextView!
    @IBOutlet var txtDescriptionArabic: UITextView!
    @IBOutlet var txtPrice: UITextField!
    @IBOutlet var txtPreviousPrice: UITextField!
    @IBOutlet var imgProduct: ScaledHeightImageView!
    @IBOutlet var lblProcutCond: UILabel!
    @IBOutlet var btnPromote: UIButton!
    
    var imagePicker = UIImagePickerController()
    var allCategories = [Categorys]()
    var categoryID: Int?
    var subCategoryID:Int?
    var childCategory:Int?
    var isNew = true
    var latitude = 0.0
    var longitude = 0.0
    var selectedPost : Int?
     var postID  = 0
    var selectedImage = false
    var type = 0
    var selectedIndex = -1
    @IBOutlet var viewCollection: [UIView]!
    @IBOutlet var viewcol: [UITextView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("pickerView", owner: self, options: nil)
        setUI()
        setLanguage()
        if selectedPost != nil{
            getProductDetail()
            //btnPromote.isHidden = true
            Bundle.main.loadNibNamed("ViewPromote", owner: self, options: nil)
        }else{
            //btnPromote.isHidden = true
            btnNew.setTitleColor(UIColor.black, for: .normal)
            btnUsed.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.btnNew.setImage(UIImage.init(systemName: "checkmark.circle"), for: .normal)
            self.btnUsed.setImage(nil, for: .normal)
            
            viewSubCategory.isHidden = true
            viewChildCategory.isHidden = true
             
            txtSpecification.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "spec", comment: "") + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")
 
            txtSpecification.textColor = UIColor.lightGray
            txtSpecification.textColor = UIColor.lightGray
            txtSpecification.selectedTextRange = txtSpecification.textRange(from: txtSpecification.beginningOfDocument, to: txtSpecification.beginningOfDocument)
            txtSpecification.delegate = self
             
            txtSpecificationArabic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "spec_ar", comment: "")
 
            txtSpecificationArabic.textColor = UIColor.lightGray
            txtSpecificationArabic.textColor = UIColor.lightGray
            txtSpecificationArabic.selectedTextRange = txtSpecificationArabic.textRange(from: txtSpecificationArabic.beginningOfDocument, to: txtSpecificationArabic.beginningOfDocument)
            txtSpecificationArabic.delegate = self
             
            txtDescription.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "post_des", comment: "")  + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")
 
            txtDescription.textColor = UIColor.lightGray
            txtDescription.textColor = UIColor.lightGray
            txtDescription.selectedTextRange = txtDescription.textRange(from: txtDescription.beginningOfDocument, to: txtDescription.beginningOfDocument)
            txtDescription.delegate = self
             
            txtDescriptionArabic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "post_des_ar", comment: "")
 
            txtDescriptionArabic.textColor = UIColor.lightGray
            txtDescriptionArabic.textColor = UIColor.lightGray
            txtDescriptionArabic.selectedTextRange = txtDescriptionArabic.textRange(from: txtDescriptionArabic.beginningOfDocument, to: txtDescriptionArabic.beginningOfDocument)
            txtDescriptionArabic.delegate = self
             
//            determineMyCurrentLocation()
        }
        
        
    }
    
    func setLanguage(){
        btnUpload.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "upload", comment: "")
, for: .normal)
        txtProductName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_name", comment: "")
        txtProductNameArabic.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_name_ar", comment: "")
        lblProcutCond.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_cond", comment: "")
        txtProductCondition.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_cond", comment: "")


        btnNew.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: ""), for: .normal)
        btnUsed.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: ""), for: .normal)
        txtCategory.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "category_name", comment: "")
        txtSubCategory.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "subCategory_name", comment: "")
        txtChildCategory.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "childCategory_name", comment: "")
        txtBrand.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "brand", comment: "") + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")
        txtBrandArabic.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bramd_arabic", comment: "")
        txtModel.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "model", comment: "") + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")
        txtModelArabic.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "model_ar", comment: "")
        txtLocation.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "") + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")
        txtLocationArabic.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location_ar", comment:
            "")
        txtPrice.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "price", comment: "")
        txtPreviousPrice.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "prev_price", comment: "")
        btnSubmit.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "submit", comment: "")
, for: .normal)
        
         if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            txtProductName.textAlignment = .right
            txtProductCondition.textAlignment = .right
            txtCategory.textAlignment = .right
            txtSubCategory.textAlignment = .right
            txtChildCategory.textAlignment = .right
            txtBrand.textAlignment = .right
            txtModel.textAlignment = .right
            txtSpecification.textAlignment = .right
            txtLocation.textAlignment = .right
            txtDescription.textAlignment = .right
            txtPrice.textAlignment = .right
         }else{
            txtProductName.textAlignment = .left
            txtProductCondition.textAlignment = .left
            txtCategory.textAlignment = .left
            txtSubCategory.textAlignment = .left
            txtChildCategory.textAlignment = .left
            txtBrand.textAlignment = .left
            txtModel.textAlignment = .left
            txtSpecification.textAlignment = .left
            txtLocation.textAlignment = .left
            txtDescription.textAlignment = .left
            txtPrice.textAlignment = .left
        }
        
    }
    
    func setUI(){
        for all in viewCollection{
            all.layer.cornerRadius = 5
            all.layer.masksToBounds = true
            all.layer.borderWidth = 0.5
            all.layer.borderColor = UIColor.lightGray.cgColor
        }
        
        for all in viewcol{
            all.layer.cornerRadius = 5
            all.layer.masksToBounds = true
            all.layer.borderWidth = 0.5
            all.layer.borderColor = UIColor.lightGray.cgColor
        }
        
        self.txtProductNameArabic.textAlignment = NSTextAlignment.right
        self.txtBrandArabic.textAlignment = NSTextAlignment.right
        self.txtModelArabic.textAlignment = NSTextAlignment.right
        self.txtSpecificationArabic.textAlignment = NSTextAlignment.right
        self.txtLocationArabic.textAlignment = NSTextAlignment.right
        self.txtDescriptionArabic.textAlignment = NSTextAlignment.right

    }
    
    func getProductDetail(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
              loadingview?.modalPresentationStyle = .fullScreen
              loadingview?.loadWith(parent: self)
        
        ClassifiedAPIFacade.shared.getProductDetails(prodID: selectedPost!) { (aRes) in
                  DispatchQueue.main.async {
                      loadingview?.removeFromParentView(self)
                  }
                  if aRes?.statusCode == 200{
                      if let data = aRes?.json?["data"].dictionary{
                        self.txtProductName.text = data["name"]?.stringValue ?? ""
                        self.txtProductNameArabic.text = data["name_ar"]?.stringValue ?? ""
                        self.txtPrice.text = "\(data["price"]?.intValue ?? 0)"
                        self.txtSpecification.text =  data["specification"]?.stringValue ?? ""
                        self.txtSpecificationArabic.text = data["specification_ar"]?.stringValue ?? ""
                        self.txtDescription.text = data["details"]?.stringValue ?? ""
                        self.txtDescriptionArabic.text = data["details"]?.stringValue ?? ""
                        if Int(data["product_condition"]?.stringValue ?? "0") ?? 0 == 0{
                            self.isNew = false
                            self.btnNew.setTitleColor(UIColor.lightGray, for: .normal)
                            self.btnUsed.setTitleColor(UIColor.black, for: .normal)
                            
                            self.btnNew.setImage(nil, for: .normal)
                            self.btnUsed.setImage(UIImage.init(systemName: "checkmark.circle"), for: .normal)
                            self.txtProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                        }else{
                            self.isNew = true
                            self.btnNew.setTitleColor(UIColor.black, for: .normal)
                            self.btnUsed.setTitleColor(UIColor.lightGray, for: .normal)
                            
                            self.btnNew.setImage(UIImage.init(systemName: "checkmark.circle"), for: .normal)
                            self.btnUsed.setImage(nil, for: .normal)
                            self.txtProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: "")
                        }
                        self.imgProduct.kf.setImage(with: setImage(imageURL: data["photo"]?.stringValue ?? ""))
                        if data["photo"]?.stringValue != ""{
                            self.selectedImage = true
                        }else{
                            self.selectedImage = false
                        }
                        
                        self.txtPreviousPrice.text = "\(data["previous_price"]?.intValue ?? 0)"
                        self.txtBrand.text = data["brand"]?.stringValue ?? ""
                        self.txtBrandArabic.text = data["brand_ar"]?.stringValue ?? ""
                        self.txtModel.text = data["model"]?.stringValue ?? ""
                        self.txtModelArabic.text = data["model_ar"]?.stringValue ?? ""
                        self.txtLocation.text = data["location"]?.stringValue ?? ""
                        self.txtLocationArabic.text = data["location_ar"]?.stringValue ?? ""
                       
                        self.categoryID = data["category_id"]?.intValue ?? 0
                        self.subCategoryID = data["subcategory_id"]?.intValue ?? 0
                        self.childCategory = data["childcategory_id"]?.intValue ?? 0
                        
                        self.txtCategory.text = data["category_name"]?.stringValue ?? ""
                        self.txtSubCategory.text = data["subcategory_name"]?.stringValue ?? ""
                        self.txtChildCategory.text = data["childcategory_name"]?.stringValue ?? ""
                        self.postID = data["post_id"]?.intValue ?? 0
                        
                        self.viewSubCategory.isHidden = true
                        self.viewChildCategory.isHidden = true
                       
                        if self.subCategoryID != 0 && self.subCategoryID != nil{
                            self.viewSubCategory.isHidden = false
                            self.viewChildCategory.isHidden = true
                        }
                        if self.childCategory != 0 && self.childCategory != nil{
                           self.viewChildCategory.isHidden = false
                        }
                        
                    }
                  }else{
                     showNetworkError(sender: self)
                  }
              }
    }
    
    
    
//    func determineMyCurrentLocation() {
//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestAlwaysAuthorization()
//
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.startUpdatingLocation()
//                  //locationManager.startUpdatingHeading()
//        }
//    }
    
    func showPicker(){
        pickerView.frame = self.view.bounds
        self.view.addSubview(pickerView)
        self.view.bringSubviewToFront(pickerView)
//        self.pickerView.transform = self.pickerView.transform.scaledBy(x: 0.001, y: 0.001)
//        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
//                          self.pickerView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
//        }, completion: nil)
        
        self.picker.reloadAllComponents()

    }
    
    func getCategoryList(sender:UIButton){
        self.type = 0
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
                
        ClassifiedAPIFacade.shared.getCategories { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
        //                if aRes?.json?["success"].intValue == 0{
        //                    showAlert(title: "", subTitle: aRes?.json?["msg"].stringValue ?? "", sender: self)
        //                }else{
                self.allCategories.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Categorys(json: m)
                    self.allCategories.append(booking)
                }
                    }
                //self.showPopOver(array: self.allCategories, sender: sender, type: 0)
                self.showPicker()
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    func getSubCategoryList(sender:UIButton){
        self.type = 1
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .overFullScreen
        loadingview?.loadWith(parent: self)
                
        ClassifiedAPIFacade.shared.getSubCategories(categoryID: categoryID!) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.allCategories.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Categorys(json: m)
                    self.allCategories.append(booking)
                }
                    }
                self.showPicker()
                //self.showPopOver(array: self.allCategories, sender: sender, type: 1)
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    func getChildCategory(sender:UIButton){
        self.type = 2
           let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
           loadingview?.modalPresentationStyle = .fullScreen
           loadingview?.loadWith(parent: self)
                   
           ClassifiedAPIFacade.shared.getChildCategory(subCatId: subCategoryID!) { (aRes) in
               DispatchQueue.main.async {
                   loadingview?.removeFromParentView(self)
               }
               if aRes?.statusCode == 200{
                   self.allCategories.removeAll()
                   if let data = aRes?.json?["data"].array{
                   for m in data{
                       let booking = Categorys(json: m)
                       self.allCategories.append(booking)
                   }
                       }
                self.showPicker()
                //self.showPopOver(array: self.allCategories, sender: sender, type: 2)
               }else{
                   showNetworkError(sender: self)
               }
           }
       }
    
    func showCOndition(){
         self.type = 3
        self.showPicker()
    }
    
    func showPopOver(array:[Categorys],sender:UIButton,type:Int){
        var arrayList = [String]()
        for all in array{
            arrayList.append(all.name ?? "") //arabic
        }
        
        let configuration = FTConfiguration.shared
        configuration.menuWidth = self.view.frame.width-20
        configuration.borderWidth = 1
        configuration.borderColor = UIColor.lightGray
        configuration.backgoundTintColor = UIColor.init(red: 15.0/255.0, green: 18.0/255.0, blue: 63.0/255.0, alpha: 0.8)
        
        FTPopOverMenu.showForSender(sender:  sender,
                                    with: arrayList,
                                    done: { (selectedIndex) -> () in
                                        if type == 0{
                                            self.txtCategory.text = arrayList[selectedIndex]
                                            self.categoryID = array[selectedIndex].id
                                            if array[selectedIndex].sub_cat_count ?? 0 >= 1{
                                                self.viewSubCategory.isHidden = false
                                            }else{
                                                self.viewSubCategory.isHidden = true
                                                self.viewChildCategory.isHidden = true
                                                self.subCategoryID = 0
                                                self.childCategory = 0
                                            }
                                        }else if type == 1{
                                            self.txtSubCategory.text = arrayList[selectedIndex]
                                            self.subCategoryID = array[selectedIndex].id
                                            if array[selectedIndex].child_cat_count ?? 0 >= 1{
                                                 self.viewChildCategory.isHidden = false
                                            }else{
                                                self.viewChildCategory.isHidden = true
                                                self.childCategory = 0
                                            }
                                        }else{
                                            self.txtChildCategory.text = arrayList[selectedIndex]
                                            self.childCategory = array[selectedIndex].id
                                        }
        }, cancel: {
            
        })
    }
    
    func uploadPost(){
        var detail = [String:Any]()
        detail["name"] = txtProductName.text ?? ""
        detail["name_ar"] = txtProductNameArabic.text ?? ""
        detail["product_condition"] = isNew == true ? "New" : "Used"
        detail["category_id"] = categoryID
        if subCategoryID != nil && subCategoryID != 0{
            detail["subcategory_id"] = subCategoryID
        }
        
        if childCategory != nil && childCategory != 0{
            detail["childcategory_id"] = childCategory
        }
        detail["brand"] = txtBrand.text ?? ""
        detail["brand_ar"] = txtBrandArabic.text ?? ""
        detail["model"] = txtModel.text ?? ""
        detail["model_ar"] = txtModelArabic.text ?? ""
        if txtSpecification.textColor == UIColor.black{
           detail["specification"] = txtSpecification.text ?? ""
        }
        if txtSpecificationArabic.textColor == UIColor.black{
           detail["specification_ar"] = txtSpecificationArabic.text ?? ""
        }
        
        detail["location"] = txtLocation.text ?? ""
        detail["location_ar"] = txtLocationArabic.text ?? ""
        if txtDescription.textColor == UIColor.black{
           detail["details"] = txtDescription.text ?? ""
        }
        if txtDescriptionArabic.textColor == UIColor.black{
            detail["details_ar"] = txtDescriptionArabic.text ?? ""
        }
        
        detail["price"] = txtPrice.text ?? ""
        detail["previous_price"] = txtPreviousPrice.text ?? ""
        detail["long"] = "1"
        detail["lat"] = "1"
        
        if selectedImage == true{
            if imgProduct.image != nil {
                             //let resize = resizeImage(image: imgProduct.image!, targetSize: CGSize(width: 640, height: 480))

                       if let imgData:Data = imgProduct.image?.pngData() {
                                 detail["photo"] = imgData
                             }
                         }
                         else {
                             detail["photo"] = ""
                         }
        }
        
        if selectedPost != nil{
            detail["post_id"] = self.postID
            uploadPost(detail: detail, isEdit: true)
        }else{
            uploadPost(detail: detail, isEdit: false)
        }
    }
    
    func uploadPost(detail:[String:Any],isEdit:Bool){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
                     loadingview?.loadWith(parent: self)
        
        if isEdit == false{
            ClassifiedAPIFacade.shared.uploadPost(details: detail) {(aResp) in
                               DispatchQueue.main.async {
                                   loadingview?.removeFromParentView(self)
                               }
                               if aResp?.statusCode == 200{
                                   if aResp?.json?["status"].intValue == 200{
                                       let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue ?? "", preferredStyle: .alert)
                                       let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
            ,style: .cancel) { (alert) -> Void in
                                           self.dismiss(animated: true, completion: nil)
                                       }
                                       alert.addAction(OkButton)
                                       self.present(alert, animated: true, completion: nil)
                                       
                                   }else{
                                       showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                                   }
                               }else{
                                  showNetworkError(sender: self)
                               }
                           }
        }else{
            
            ClassifiedAPIFacade.shared.ediPost(details: detail) {(aResp) in
                               DispatchQueue.main.async {
                                   loadingview?.removeFromParentView(self)
                               }
                               if aResp?.statusCode == 200{
                                   if aResp?.json?["status"].intValue == 200{
                                       let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue ?? "", preferredStyle: .alert)
                                       let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
            ,style: .cancel) { (alert) -> Void in
                                           self.dismiss(animated: true, completion: nil)
                                       }
                                       alert.addAction(OkButton)
                                       self.present(alert, animated: true, completion: nil)
                                       
                                   }else{
                                       showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                                   }
                               }else{
                                  showNetworkError(sender: self)
                               }
                           }
        }
                       
    }
    
    @IBAction func btnSelectCondition(_ sender: UIButton) {
        showCOndition()
    }
    
    @IBAction func btnSelectCategory(_ sender: UIButton) {
        if sender.tag == 10{
            if selectedIndex == -1{
                       selectedIndex = 0
                   }
                   if type == 0{
                       self.txtCategory.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[selectedIndex].name_ar :  allCategories[selectedIndex].name
                       self.categoryID = allCategories[selectedIndex].id
                   if allCategories[selectedIndex].sub_cat_count ?? 0 >= 1{
                       self.viewSubCategory.isHidden = false
                   }else{
                       self.viewSubCategory.isHidden = true
                       self.viewChildCategory.isHidden = true
                       self.subCategoryID = 0
                       self.childCategory = 0
                       }
                   }else if type == 1{
                       self.txtSubCategory.text =  LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[selectedIndex].name_ar :  allCategories[selectedIndex].name
                       self.subCategoryID = allCategories[selectedIndex].id
                       if allCategories[selectedIndex].child_cat_count ?? 0 >= 1{
                           self.viewChildCategory.isHidden = false
                       }else{
                           self.viewChildCategory.isHidden = true
                           self.childCategory = 0
                       }
                   }else if type == 2{
                       self.txtChildCategory.text = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[selectedIndex].name_ar :  allCategories[selectedIndex].name
                       self.childCategory = allCategories[selectedIndex].id
                   }else{
                    if selectedIndex == 0{
                        self.txtProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: "")
                        self.isNew = true
                    }else{
                         self.txtProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                        self.isNew = false
                    }
                }
                   pickerView.removeFromSuperview()
                   
                   selectedIndex = -1
        }else{
            pickerView.removeFromSuperview()
            selectedIndex = -1
        }
       
    }
    
    @IBAction func btnPromote(_ sender: UIButton) {
            viewPromot.frame = self.view.bounds
            self.view.addSubview(viewPromot)
            self.view.bringSubviewToFront(viewPromot)
//               self.viewPromot.transform = self.viewPromot.transform.scaledBy(x: 0.001, y: 0.001)
//                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
//                    self.viewPromot.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
//               }, completion: nil)
    }
    
    @IBAction func btnMakeBannerAd(_ sender: UIButton) {
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
              loadingview?.modalPresentationStyle = .fullScreen
              loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.makeBannerAd(postID: selectedPost! ){ (aResp) in
                  DispatchQueue.main.async {
                      loadingview?.removeFromParentView(self)
                  }
                  if aResp?.statusCode == 200{
                      if aResp?.json?["status"].intValue == 200{
                         let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                           let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                       ,style: .cancel) { (alert) -> Void in
                                                                                self.viewPromot.removeFromSuperview()
                                                                           }
                                                                           alert.addAction(OkButton)
                                                                           self.present(alert, animated: true, completion: nil)
                      }else{
                        let alert = UIAlertController(title: "Oops!" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                                  let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                                              ,style: .cancel) { (alert) -> Void in
                                                                                                       self.viewPromot.removeFromSuperview()
                                                                                                  }
                                                                                                  alert.addAction(OkButton)
                                                                                                  self.present(alert, animated: true, completion: nil)
                        
                      }
                  }else{
                    showNetworkError(sender: self)
                  }
              }
    }
    
    @IBAction func btnMakeFeature(_ sender: UIButton) {
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
                     loadingview?.modalPresentationStyle = .fullScreen
                     loadingview?.loadWith(parent: self)

        ClassifiedAPIFacade.shared.makeFeatureAd(postID: selectedPost!){ (aResp) in
                         DispatchQueue.main.async {
                             loadingview?.removeFromParentView(self)
                         }
                         if aResp?.statusCode == 200{
                             if aResp?.json?["status"].intValue == 200{
                                  let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                    let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                ,style: .cancel) { (alert) -> Void in
                                                         self.viewPromot.removeFromSuperview()
                                                    }
                                                    alert.addAction(OkButton)
                                                    self.present(alert, animated: true, completion: nil)
                                
                               
                             }else{
                                   let alert = UIAlertController(title: "Oops!" , message: aResp?.json?["message"].stringValue                                , preferredStyle: .alert)
                                                                                                                                let OkButton = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: "")
                                                                                                            ,style: .cancel) { (alert) -> Void in
                                                                                                                                     self.viewPromot.removeFromSuperview()
                                                                                                                                }
                                                                                                                                alert.addAction(OkButton)
                                                                                                                                self.present(alert, animated: true, completion: nil)
                                                      
                             }
                         }else{
                           showNetworkError(sender: self)
                         }
                     }
    }
    
    @IBAction func btnCancelPromote(_ sender: UIButton) {
        viewPromot.removeFromSuperview()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCondition(_ sender: UIButton) {
        animationScaleEffect(view: sender)
        if sender.tag == 10{
            isNew = true
            btnNew.setTitleColor(UIColor.black, for: .normal)
            btnUsed.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.btnNew.setImage(UIImage.init(systemName: "checkmark.circle"), for: .normal)
            self.btnUsed.setImage(nil, for: .normal)
            
        }else{
            isNew = false
            btnUsed.setTitleColor(UIColor.black, for: .normal)
            btnNew.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.btnNew.setImage(nil, for: .normal)
            self.btnUsed.setImage(UIImage.init(systemName: "checkmark.circle"), for: .normal)
        }
    }
    
    @IBAction func btnUploadImage(_ sender: UIButton) {
        animationScaleEffect(view: sender)
        
        var config = YPImagePickerConfiguration()
        config.filters = []
        config.library.mediaType = YPlibraryMediaType.photo
        
        
        let picker = YPImagePicker()
          picker.didFinishPicking { [unowned picker] items, _ in
              if let photo = items.singlePhoto {
                  print(photo.fromCamera) // Image source (camera or library)
                  print(photo.image) // Final image selected by the user
                  print(photo.originalImage) // original image selected by the user, unfiltered
                  print(photo.modifiedImage) // Transformed image, can be nil
                  print(photo.exifMeta) // Print exif meta data of original image.
                self.imgProduct.contentMode = .scaleAspectFit
                self.selectedImage = true
              //  let image = resizeImage(image: photo.image, targetSize: self.imgProduct.frame.size)
                self.imgProduct.image = photo.image
                self.imgProduct.clipsToBounds = true
              }
              picker.dismiss(animated: true, completion: nil)
          }
          self.present(picker, animated: true, completion: nil)
        
//        let alertController = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "upload_propduct", comment: "")
//, message: "", preferredStyle: .actionSheet)
//
//        let Camera = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "use_camera", comment: "")
//, style: .default) { action in
//
//
//
//
//
//            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
//                self.imagePicker.delegate = self
//                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera;
//                self.imagePicker.videoQuality = .typeMedium
//                self.imagePicker.allowsEditing = true
//
//                self.present(self.imagePicker, animated: true, completion: nil)
//
//            }else{
//                let alert = UIAlertController(title: "" , message: "CameraNotFound", preferredStyle: .alert)
//                let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
//                }
//                alert.addAction(OkButton)
//                self.present(alert, animated: true, completion: nil)
//
//            }
//        }
//
//        let Gallery = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "use_gallery", comment: "")
//, style: .default){ action in
//
//            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
//
//                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
//                    self.imagePicker.delegate = self
//                    self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
//                    self.imagePicker.allowsEditing = true
//                    self.imagePicker.videoQuality = .typeMedium
//                    self.present(self.imagePicker, animated: true, completion: nil)
//
//                }            }else
//            {
//                let alert = UIAlertController(title: "", message: "Can'tLocatePhotoLibrary", preferredStyle: .alert)
//                let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
//                }
//                alert.addAction(OkButton)
//                self.present(alert, animated: true, completion: nil)
//
//            }
//
//
//        }
//        let Cancel = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "")
//, style: .default)
//        alertController.addAction(Camera)
//        alertController.addAction(Gallery)
//        alertController.addAction(Cancel)
//        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            imgProduct.contentMode = .scaleAspectFill
            imgProduct.image = image
            selectedImage = true
        }
        else if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
            imgProduct.contentMode = .scaleAspectFill
            imgProduct.image = image
            selectedImage = true
        }
        else{
            print("error")
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
          return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
      }
      
      // Helper function inserted by Swift 4.2 migrator.
      fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
          return input.rawValue
      }
    
    
    @IBAction func btnCatagory(_ sender: UIButton) {
        if sender.tag == 10{
                   getCategoryList(sender: sender)
               }else if sender.tag == 20{
                   getSubCategoryList(sender: sender)
               }else{
                   getChildCategory(sender: sender)
               }
    }
    
    @IBAction func btnsubmit(_ sender: UIButton) {
         animationScaleEffect(view: sender)
        if txtProductName.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_productName", comment: "")
, sender: self)
            return
        }else if txtProductCondition.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_coondition", comment: "")
            , sender: self)
                       return
        }
//        else if txtProductNameArabic.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_productName_ar", comment: "")
//, sender: self)
//            return
//        }
        else if txtCategory.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_category", comment: "")
, sender: self)
            return
        }else if viewSubCategory.isHidden == false{
            if txtSubCategory.text == ""{
                 showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_subCategory", comment: "")
, sender: self)
                return
            }
           
        }else if viewChildCategory.isHidden == false{
            if txtChildCategory.text == ""{
                 showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_child", comment: "")
, sender: self)
                return
            }
        }
//        if txtBrand.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_brand", comment: "")
//, sender: self)
//            return
//        }
//        else if txtBrandArabic.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_brand_ar", comment: "")
//, sender: self)
//            return
//        }
//        else if txtModel.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_model", comment: "")
//, sender: self)
//            return
//        }
//        else if txtModelArabic.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_mode_ar", comment: "")
//, sender: self)
//            return
//        }
//        else if txtSpecification.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_spec", comment: "")
//, sender: self)
//            return
//        }
//        else if txtSpecificationArabic.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_spce_ar", comment: "")
// , sender: self)
//            return
//        }
//        else if txtLocation.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_loc", comment: "")
// , sender: self)
//            return
//        }
//        else if txtLocationArabic.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_loc_arabic", comment: "")
// , sender: self)
//            return
//        }
//        else if txtDescription.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_des", comment: "")
// , sender: self)
//            return
//        }
//        else if txtDescriptionArabic.text == ""{
//            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_des_ar", comment: "")
// , sender: self)
//            return
//        }
        else if txtPrice.text == ""{
            showAlert(title: "", subTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_price", comment: "")
 , sender: self)
            return
        }
        
        uploadPost()
    }

}

extension UploadViewController : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

           // Combine the textView text and the replacement text to
           // create the updated text string
          
        
            let currentText:String = textView.text
           let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

           // If updated text view will be empty, add the placeholder
           // and set the cursor to the beginning of the text view
        
        switch textView {
        case txtSpecification:
            if updatedText.isEmpty {

                          txtSpecification.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "spec", comment: "") + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")

                          txtSpecification.textColor = UIColor.lightGray

                          txtSpecification.selectedTextRange = textView.textRange(from: txtSpecification.beginningOfDocument, to: txtSpecification.beginningOfDocument)
                      }

                      // Else if the text view's placeholder is showing and the
                      // length of the replacement string is greater than 0, set
                      // the text color to black then set its text to the
                      // replacement string
                       else if txtSpecification.textColor == UIColor.lightGray && !text.isEmpty {
                          txtSpecification.textColor = UIColor.black
                          txtSpecification.text = text
                      }

                      // For every other case, the text should change with the usual
                      // behavior...
                      else {
                          return true
                      }
            break
        case txtSpecificationArabic:
            if updatedText.isEmpty {

                      txtSpecificationArabic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "spec_ar", comment: "") + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")

                      txtSpecificationArabic.textColor = UIColor.lightGray

                      txtSpecificationArabic.selectedTextRange = textView.textRange(from: txtSpecificationArabic.beginningOfDocument, to: txtSpecificationArabic.beginningOfDocument)
                  }

                  // Else if the text view's placeholder is showing and the
                  // length of the replacement string is greater than 0, set
                  // the text color to black then set its text to the
                  // replacement string
                   else if txtSpecificationArabic.textColor == UIColor.lightGray && !text.isEmpty {
                      txtSpecificationArabic.textColor = UIColor.black
                      txtSpecificationArabic.text = text
                  }

                  // For every other case, the text should change with the usual
                  // behavior...
                  else {
                      return true
                  }
        break
        
        case txtDescription:
            if updatedText.isEmpty {

                      txtDescription.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "post_des", comment: "") + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")

                      txtDescription.textColor = UIColor.lightGray

                      txtDescription.selectedTextRange = textView.textRange(from: txtDescription.beginningOfDocument, to: txtDescription.beginningOfDocument)
                  }

                  // Else if the text view's placeholder is showing and the
                  // length of the replacement string is greater than 0, set
                  // the text color to black then set its text to the
                  // replacement string
                   else if txtDescription.textColor == UIColor.lightGray && !text.isEmpty {
                      txtDescription.textColor = UIColor.black
                      txtDescription.text = text
                  }

                  // For every other case, the text should change with the usual
                  // behavior...
                  else {
                      return true
                  }
        break
            
        case txtDescriptionArabic:
            if updatedText.isEmpty {

                      txtDescriptionArabic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "post_des_ar", comment: "")  + LocalizationSystem.sharedInstance.localizedStringForKey(key: "optnal", comment: "")

                      txtDescriptionArabic.textColor = UIColor.lightGray

                      txtDescriptionArabic.selectedTextRange = textView.textRange(from: txtDescriptionArabic.beginningOfDocument, to: txtDescriptionArabic.beginningOfDocument)
                  }

                  // Else if the text view's placeholder is showing and the
                  // length of the replacement string is greater than 0, set
                  // the text color to black then set its text to the
                  // replacement string
                   else if txtDescriptionArabic.textColor == UIColor.lightGray && !text.isEmpty {
                      txtDescriptionArabic.textColor = UIColor.black
                      txtDescriptionArabic.text = text
                  }

                  // For every other case, the text should change with the usual
                  // behavior...
                  else {
                      return true
                  }
        break
        default:
            print("invalid")
        }
          

           // ...otherwise return false since the updates have already
           // been made
           return false
       }
}

//extension UploadViewController:CLLocationManagerDelegate{
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let location:CLLocation = locations[0] as CLLocation
//
//        latitude = location.coordinate.longitude
//        longitude = location.coordinate.longitude
//
//        //Finally stop updating location otherwise it will come again and again in this delegate
//
//        self.locationManager.stopUpdatingLocation()
//
//    }
//
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
//    {
//        print("Error \(error)")
//    }
//}
extension UploadViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if type == 3{
            return 2
        }else{
            return allCategories.count
        }
         
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if type == 3{
            if row == 0{
              return LocalizationSystem.sharedInstance.localizedStringForKey(key: "new", comment: "")
            }else{
              return LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
            }
        }else{
            return  LocalizationSystem.sharedInstance.getLanguage() == "ar" ? allCategories[row].name_ar :  allCategories[row].name
        }
       
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
//        gradeTextField.text = allCategories[row]
        selectedIndex = row
        self.view.endEditing(true)
    }
}
