//
//  CustomeTabBar.swift
//  Classified
//
//  Created by WC_Macmini on 23/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class CustomeTabBar: UITabBarController {
    
    var tabItem = UITabBarItem()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        customTab(selectedImage: "home_select", deselectedImage: "home", indexOfTab: 0 , tabTitle: "")
        customTab(selectedImage: "notif_select", deselectedImage: "notification", indexOfTab: 1 , tabTitle: "")
        customTab(selectedImage: "explore_selec", deselectedImage: "explore", indexOfTab: 2 , tabTitle: "")
        customTab(selectedImage: "saved_selec", deselectedImage: "saved_list", indexOfTab: 3 , tabTitle: "")
        customTab(selectedImage: "profile_select", deselectedImage: "profile", indexOfTab: 4 , tabTitle: "")
    }
    

     func customTab(selectedImage image1 : String , deselectedImage image2: String , indexOfTab index: Int , tabTitle title: String ){
              
              let selectedImage = UIImage(named: image1)!.withRenderingMode(.alwaysOriginal)
              let deselectedImage = UIImage(named: image2)!.withRenderingMode(.alwaysOriginal)
              tabItem = self.tabBar.items![index]
              tabItem.image = deselectedImage
              tabItem.selectedImage = selectedImage
              tabItem.title = .none
              tabItem.imageInsets.bottom = -11
              
          }
}
