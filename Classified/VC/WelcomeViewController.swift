//
//  WelcomeViewController.swift
//  Classified
//
//  Created by WC_Macmini on 29/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet var btnArabic: UIButton!
    @IBOutlet var btnEnglish: UIButton!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var btnDone: UIButton!
    
    var isFromSettings = false
    var isEnglish = true
    override func viewDidLoad() {
        super.viewDidLoad()

        btnEnglish.setTitleColor(UIColor.white, for: .normal)
        btnEnglish.backgroundColor = UIColor.init(hexString: "#3CBCDD")
        
        btnArabic.setTitleColor(UIColor.darkGray, for: .normal)
        btnArabic.backgroundColor = UIColor.white
        
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = UIColor.init(hexString: "#3CBCDD")?.cgColor
        
        btnArabic.setTitle("عربى", for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            btnEnglish.setTitleColor(UIColor.darkGray, for: .normal)
            btnEnglish.backgroundColor = UIColor.white
                   
            btnArabic.setTitleColor(UIColor.white, for: .normal)
            btnArabic.backgroundColor = UIColor.init(hexString: "#3CBCDD")
            isEnglish = false
        }else{
            isEnglish = true
            btnEnglish.setTitleColor(UIColor.white, for: .normal)
            btnEnglish.backgroundColor = UIColor.init(hexString: "#3CBCDD")
                   
            btnArabic.setTitleColor(UIColor.darkGray, for: .normal)
            btnArabic.backgroundColor = UIColor.white
        }
    }

    @IBAction func btnChangeLanguage(_ sender: UIButton) {
        if sender.tag == 10{
            btnEnglish.setTitleColor(UIColor.white, for: .normal)
            btnEnglish.backgroundColor = UIColor.init(hexString: "#3CBCDD")
            btnArabic.setTitleColor(UIColor.darkGray, for: .normal)
            btnArabic.backgroundColor = UIColor.white
            isEnglish = true
            }
        else{
            isEnglish = false
            btnArabic.setTitleColor(UIColor.white, for: .normal)
            btnArabic.backgroundColor = UIColor.init(hexString: "#3CBCDD")
            btnEnglish.setTitleColor(UIColor.darkGray, for: .normal)
            btnEnglish.backgroundColor = UIColor.white
            }
        }
    
    @IBAction func btnDone(_ sender: UIButton) {
        if isEnglish{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        if isFromSettings == true{
            let homeView = storyboard?.instantiateViewController(withIdentifier: storyBrdIds.homeView)
            homeView?.modalPresentationStyle = .fullScreen
            self.present(homeView!, animated: true, completion: nil)
        }else{
            changeFirstTime()
            let vc = self.storyboard?.instantiateViewController(identifier: storyBrdIds.homeView)
            vc?.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: true, completion: nil)
        }
        
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
