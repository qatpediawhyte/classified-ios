//
//  WeViewViewController.swift
//  Classified
//
//  Created by WC_Macmini on 13/08/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import WebKit

class WeViewViewController: UIViewController {

    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var webView: WKWebView!
    
    var isPrivacy = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isPrivacy == 1{
            let url = URL(string: "https://saaha.qa/privacy_policy.pdf")
            webView.load(URLRequest(url: url!))
            lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "privacy_policy", comment: "")
        }else if isPrivacy == 2{
            let url = URL(string: "https://saaha.qa/terms-conditions.pdf")
            webView.load(URLRequest(url: url!))
            lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "terms", comment: "")
        }else{
            let url = URL(string: "http://whytecreations.in/beta/saaha/admin/login")
            webView.load(URLRequest(url: url!))
            lblHeading.text = ""
            lblHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "admin", comment: "")
        }
        
    }
    
    
    
    @IBAction func btnBack(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
