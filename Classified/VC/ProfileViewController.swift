//
//  ProfileViewController.swift
//  Classified
//
//  Created by WC_Macmini on 30/06/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import HCSStarRatingView
import ClassifiedWebkit

class ProfileViewController: UIViewController {

    struct VCConst {
        static let profileCellId = "profile_list_cell_id"
    }
    
    @IBOutlet var profileRating: HCSStarRatingView!
    @IBOutlet var rating: HCSStarRatingView!
    @IBOutlet var ratingView: UIView!
    @IBOutlet var imgName: UIImageView!
    @IBOutlet var lblMember: UILabel!
    @IBOutlet var memberView: UIView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnSettings: UIButton!
    @IBOutlet var tblPost:  UICollectionView!
    @IBOutlet var lblMemberHeading:  UILabel!
    
    @IBOutlet var loginView: UIView!
    @IBOutlet var blurView: UIView!
     @IBOutlet var btnRating: UIButton!
    
    var isFromSettings = false
    var postList = [Post]()
    var isSeller = false
    
    var sellerNAme = ""
    var sellerImage = ""
    var sellerMEmber = ""
    var sellerID = 0
    var sellerRating = "0"
    var selectedProductId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMemberHeading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "member", comment: "")
        Bundle.main.loadNibNamed("RatingView", owner: self, options: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       
//            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
//            let blurEffectView = UIVisualEffectView(effect: blurEffect)
//            blurEffectView.frame = view.bounds
//            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            blurView.addSubview(blurEffectView)

            loginView.isHidden = true
            
            if isSeller == true{
                btnRating.isHidden = false
                btnSettings.setImage(UIImage(named: "back_arrow"), for: .normal)
                btnBack.isHidden = true
                lblName.text = sellerNAme
                lblMember.text = sellerMEmber
                if sellerImage != ""{
                    self.imgName.kf.setImage(with: setImage(imageURL: sellerImage))
                }else{
                    self.imgName.image = UIImage(named: "profile_thumb")
                }
               
                
                if sellerRating != "" {
                    let rates = sellerRating
                if  let n = NumberFormatter().number(from: rates) {
                        self.profileRating.value = CGFloat(truncating: n)
                }
                }else{
                    self.profileRating.value = 0
                }
                
                getSellerPost()
            }else{
                 if ClassifiedAPIFacade.shared.session?.token == nil{
                //            showLogin(sender: self)
                //            if let tabBarController = self.tabBarController {
                //                tabBarController.selectedIndex = 0
                //            }
                    loginView.isHidden = false
                    return
                 }else{
                     loginView.isHidden = true
                }
                btnRating.isHidden = true
                btnSettings.setImage(UIImage(named: "side_"), for: .normal)
                btnBack.isHidden = false
                lblName.text = (getUserDetails()?["name"] ?? "") as? String
                lblMember.text = (getUserDetails()?["mamber_since"] ?? "") as? String
                if getUserDetails()?["photo"] as? String != "" && getUserDetails()?["photo"] as? String != nil{
                    imgName.kf.setImage(with: setImage(imageURL: getUserDetails()?["photo"] as? String ?? ""))
                }else{
                  self.imgName.image = UIImage(named: "profile_thumb")
                }
                
                if getUserDetails()?["rating"] as? String != "" && getUserDetails()?["rating"] as? String != nil{
                    let rates = getUserDetails()?["rating"] as? String ?? "0"
                if  let n = NumberFormatter().number(from: rates) {
                        self.profileRating.value = CGFloat(truncating: n)
                    }
                     
                }else{
                    self.profileRating.value = 0
                }
                
               
                getUserPosts()
            }
        
        if isFromSettings == true{
            btnBack.setImage(UIImage(named: "back_arrow"), for: .normal)
        }else{
            btnBack.isHidden = true
        }
    }
    
    func getUserPosts(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
        
        ClassifiedAPIFacade.shared.getProfileInfo { (aRes) in
            if aRes?.json?["status"].intValue == 200{
                saveUserDetails(userDetails: (ClassifiedAPIFacade.shared.session?.currentUser)!)
            }
        }
        ClassifiedAPIFacade.shared.listUserPosts { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.postList.removeAll()
                if let data = aRes?.json?["data"].array{
                for m in data{
                    let booking = Post(json: m)
                    self.postList.append(booking)
                }
                    }
                UIView.transition(with: self.tblPost, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblPost.reloadData()}, completion: nil)
                
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    func getSellerPost(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
        loadingview?.modalPresentationStyle = .fullScreen
        loadingview?.loadWith(parent: self)
        
        ClassifiedAPIFacade.shared.getSellerProdile(sellerID: sellerID) { (aRes) in
            DispatchQueue.main.async {
                loadingview?.removeFromParentView(self)
            }
            if aRes?.statusCode == 200{
                self.postList.removeAll()
                if let data = aRes?.json?["data"].dictionary{
                    if let postData = data["post"]?.array{
                        for m in postData{
                            let booking = Post(json: m)
                            self.postList.append(booking)
                        }
                    }
                    }
                UIView.transition(with: self.tblPost, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tblPost.reloadData()}, completion: nil)
                
            }else{
                showNetworkError(sender: self)
            }
        }
    }
    
    func addRating(){
        let loadingview = self.storyboard?.instantiateViewController(withIdentifier: storyBrdIds.indicatorStryBrdId) as? ActivityViewController
               loadingview?.modalPresentationStyle = .fullScreen
               loadingview?.loadWith(parent: self)
               
        let value:Int = Int(rating?.value ?? 0)

        
        ClassifiedAPIFacade.shared.addRating(rating: value, sellerID: sellerID) { (aResp) in
                   DispatchQueue.main.async {
                       loadingview?.removeFromParentView(self)
                   }
                   if aResp?.statusCode == 200{
                       if aResp?.json?["status"].intValue == 200{
                           let alert = UIAlertController(title: "" , message: aResp?.json?["message"].stringValue ?? "", preferredStyle: .alert)
                                         let OkButton = UIAlertAction(title: "Ok",style: .cancel) { (alert) -> Void in
                                            self.ratingView.removeFromSuperview()
                                         }
                            alert.addAction(OkButton)
                            self.present(alert, animated: true, completion: nil)
                       }else{
                           showAlert(title: "Oops!", subTitle: aResp?.json?["message"].stringValue ?? "", sender: self)
                       }
                   }else{
                     showNetworkError(sender: self)
                   }
               }
    }
    
    override func viewDidLayoutSubviews() {
        self.tabBarController?.tabBar.isHidden = false
        self.extendedLayoutIncludesOpaqueBars = false
    }
    
    @IBAction func signup(_ sender: UIButton) {
        showLogin(sender: self)
    }
    
    @IBAction func btnOpenRatingView(_ sender: UIButton) {
        ratingView.frame = self.view.bounds
        self.view.addSubview(ratingView)
        self.view.bringSubviewToFront(ratingView)
        self.rating.value = 0
       // self.rating.transform = self.rating.transform.scaledBy(x: 0.001, y: 0.001)
        
      //  UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
       //             self.rating.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
      //  }, completion: nil)

    }
    
    @IBAction func btnOpenProfile(_ sender: UIButton) {
        if isSeller == false{
            let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.editProfileVC) as? EditProfileViewController
                   vc?.modalPresentationStyle = .fullScreen
                   self.present(vc!, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSubmnitRating(_ sender: UIButton) {
        if sender.tag == 10{
          addRating()
        }else{
          ratingView.removeFromSuperview()
        }
    }
       
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        if isFromSettings == true{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func uploadItem(_ sender: UIButton) {
        showUploadView(sender: self)
    }
    
    @IBAction func searchItem(_ sender: UIButton) {
        showSearchView(sender: self)
    }
    
    @IBAction func btnSettings(_ sender: UIButton) {
        if isSeller == true{
            if self.navigationController == nil{
                self.dismiss(animated: true, completion: nil)
            }else{
               self.navigationController?.popViewController(animated: true)
            }
            
        }else{
            let homeView = storyboard?.instantiateViewController(withIdentifier: storyBrdIds.profilesettingsVc)
            homeView?.modalPresentationStyle = .fullScreen
             self.navigationController?.pushViewController(homeView!, animated: true)
        }
    }
}

extension ProfileViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCConst.profileCellId, for: indexPath) as! ProfileListCell
        cell.imgItem.kf.setImage(with: setImage(imageURL: postList[indexPath.item].photo ?? ""))
        cell.imgItem.contentMode = .scaleAspectFill
        cell.imgItem.clipsToBounds = true

        if isSeller == true{
            cell.imgWish.isHidden = true
            if postList[indexPath.item].wish_list == 0{
                cell.imgWish.image = UIImage(named: "wishList_black")
            }else{
                 cell.imgWish.image = UIImage(named: "wishListRed")
            }
        }else{
            cell.imgWish.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.width/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: storyBrdIds.productDetailVc) as! ProductDetailViewController
                  selectedProductId = postList[indexPath.row].post_id!
                  vc.selectedProductID = selectedProductId
             vc.modalPresentationStyle = .fullScreen
        if isSeller == true{
         
            vc.isFromSeller = true
            
        }else{
            vc.isFromProfile = true
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(identifier: storyBrdIds.uploadVC) as! UploadViewController
//            vc.selectedPost = postList[indexPath.row]
//            vc.modalPresentationStyle = .fullScreen
//            self.present(vc, animated: true, completion: nil)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
