//
//  ClassifiedAPIProvider.swift
//  Classified
//
//  Created by Isletsystems on 22/09/16.
//  Copyright © 2016 Classified. All rights reserved.
//

import UIKit
import Moya
import Alamofire

public struct ClassifiedAPIProviderFactory {
    
    
    static let endpointsClosure:(ClassifiedAPI)->Endpoint = { (target: ClassifiedAPI) -> Endpoint in
        
        var endpoint: Endpoint = Endpoint(
            url: url(target),
            sampleResponseClosure:{ .networkResponse(200, target.sampleData) },
            method: target.method,
            task: target.task,
            httpHeaderFields: target.headers
            //            task: target.task
        )
        
        // Sign all non-SignIn requests as they need the SessionID to proceed
        switch target {
        case .logIn,.signUp:
            return endpoint
        default:
            var header = ""
            if let token = ClassifiedAPIFacade.shared.session?.token {
                header = "\(token)"
            }
            return endpoint.adding(newHTTPHeaderFields: ["token": header,"deviceid":UIDevice.current.identifierForVendor!.uuidString])
        }
    }
    
    public static var offlineMode : Bool = false
    
    static func stubOrNot(_: ClassifiedAPI) -> Moya.StubBehavior {
        return offlineMode ? .immediate : .never //.delayed(seconds: 1.0)
    }
    
    public static func DefaultProvider() -> MoyaProvider<ClassifiedAPI> {
        return MoyaProvider<ClassifiedAPI>(endpointClosure: endpointsClosure,
                                      stubClosure: stubOrNot,
                                      plugins: ClassifiedAPIProviderFactory.plugins)
    }
    
    public static func StubbingProvider() -> MoyaProvider<ClassifiedAPI> {
        return MoyaProvider(endpointClosure: endpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
    }
    
    fileprivate struct SharedProvider {
        static var instance = ClassifiedAPIProviderFactory.DefaultProvider()
    }
    
    public static var sharedProvider: MoyaProvider<ClassifiedAPI> {
        get {
            return SharedProvider.instance
        }
        
        set (newSharedProvider) {
            SharedProvider.instance = newSharedProvider
        }
    }
    
    static var plugins: [PluginType] {
        return [
            ClassifiedAPINetworkLogger(whitelist: { (target:TargetType) -> Bool in
                switch target as! ClassifiedAPI {
                case .logIn: return true
                default: return true
                }
            }, blacklist: { (target:TargetType) -> Bool in
                switch target as! ClassifiedAPI {
                case .logIn(_): return true
                default: return false
                }
            })
        ]
    }
}
