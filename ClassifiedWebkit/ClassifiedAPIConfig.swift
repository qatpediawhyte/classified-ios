
//  ClassifiedAPIConfig.swift
//  ClassifiedManager
//
//  Created by islet on 14/12/16.
//  Copyright © 2016 islet. All rights reserved.
//

import UIKit

public class ClassifiedAPIConfig: NSObject {
    
    public struct Notifications {
        
        public static let sessionExpired = "api.sessionId.expired"
        
    }
    
public struct BaseUrl {
    public static let baseServerpath = "https://saaha.qa/manage/"
    public static let imageBaseServerpath = "https://saaha.qa/manage/"
    }
}
