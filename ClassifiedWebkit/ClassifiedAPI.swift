//
//  ClassifiedAPI.swift
//  Classified
//
//  Created by Isletsystems on 22/09/16.
//  Copyright © 2016 Classified. All rights reserved.
//

import UIKit
import Moya

public enum ClassifiedAPI {
    case signUp(profDetails:[String:Any])
    case logIn(profDetails:[String:Any])
    case getCategory
    case getSubCategory(categoryId:Int)
    case getChildCategory(subCategoryId:Int)
    case getBanner(profDetails:[String:Any])
    case listPost(profDetails:[String:Any])
    case listUserPost
    case forgotPassword(email:String)
    case updateProfile(profDetails:[String:Any])
    case changePassword(profDetails:[String:Any])
    case uploadPost(profDetails:[String:Any])
    case editPost(profDetails:[String:Any])
    case getProductDetail(productID:Int)
    case savePost(productID:Int,save:Int)
    case writeReview(profDetails:[String:Any])
    case getSellerProfile(sellerID:Int)
    case getSavedList
    case getSubScriptionList
    case getNotificationList
    case makeBannerAdd(prodId:Int)
    case makeFeature(prodId:Int)
    case makeSubScribe(prodId:Int)
    case removeAllSavedList
    case deletePost(prodId:Int)
    case deleteNotification(notfcnId:Int)
    case deleteAllNotification
    case addRating(rating:Int,sellerID:Int)
    case addProductRating(rating:Int,productId:Int)
    case addNewDevice(deviceID:String)
    case getProfileInfo
    case getPlanDetails
    case reportComment(profDetails:[String:Any])
    case deleteComment(commentId:Int)
    case subScribeSuccess(subScribeId:Int)
}

extension ClassifiedAPI: TargetType {
    
    public var headers: [String : String]? {
        return nil
    }
    
    var base: String {
        return  ClassifiedAPIConfig.BaseUrl.baseServerpath + "api/v1/"
        
    }
    
    public var baseURL: URL {
        return URL(string: base)!
    }

    public var path: String {
        switch self {
        case .signUp(_):
            return "signUp"
        case .logIn(_):
            return "login"
        case .getCategory:
            return "getCategories"
        case .getSubCategory(_):
            return "getSubCategories"
        case .getChildCategory(_):
            return "getChildCategories"
        case .getBanner(_):
             return "getBanners"
        case .listPost(_):
            return "list_post"
        case .listUserPost:
            return "user_post"
        case .forgotPassword(_):
            return "forgot"
        case .updateProfile(_):
            return "updateBasicInfo"
        case.changePassword(_):
            return "changePassword"
        case.uploadPost(_):
            return "add_post"
        case .editPost(_):
            return "edit_post"
        case .getProductDetail(_):
            return "post_details"
        case .savePost(_):
            return "savePost"
            
        case .writeReview(_):
            return "post_reviews"
        case .getSellerProfile(_):
            return "seller_profile"
        case .getSavedList:
            return "savePostList"
        case .getSubScriptionList:
            return "subscriptionList"
        case .getNotificationList:
            return "notification_list"
        case .makeBannerAdd(_):
            return "makeBannerPost"
        case .makeFeature(_):
            return "makeFeaturedPost"
        case .makeSubScribe(_):
            return "makeSubscribe"
        case .removeAllSavedList:
            return "removeAllSavePost"
        case .deletePost(_):
            return "delete_post"
        case .deleteNotification(_):
            return "removeSingleNotification"
        case .deleteAllNotification:
            return "removeAllNotification"
        case .addRating(_,_):
            return "addRatings"
        case .addProductRating(_,_):
            return "product_ratings"
        case .addNewDevice(_):
            return "add_new_device"
        case .getProfileInfo:
            return "getBasicInfo"
        case .getPlanDetails:
            return "plan_details"
        case .reportComment:
            return "report_comment"
        case .deleteComment:
             return "delete_comment"
        case .subScribeSuccess:
            return "makeSuccessSubscribe"

        }
        
    }
    
    public var method: Moya.Method {
        switch self {
        case    .signUp(_),
                .logIn(_),
                .getSubCategory(_),
                .getChildCategory(_),
                .getBanner(_),
                .listPost(_),
                .listUserPost,
                .forgotPassword(_),
                .updateProfile(_),
                .changePassword(_),
                .uploadPost,
                .getProductDetail(_),
                .savePost(_,_),
                .writeReview(_),
                .getSellerProfile(_),
                .editPost(_),
                .getSavedList,
                .makeFeature(_),
                .makeBannerAdd(_),
                .makeSubScribe(_),
                .deletePost(_),
                .deleteNotification(_),
                .addRating(_,_),
                .addProductRating(_,_),
                .addNewDevice(_),
                .reportComment(_),
                .deleteComment(_),
                .subScribeSuccess(_):
            return .post
        case
            .getCategory,
            .getSubScriptionList,
            .getNotificationList,
            .removeAllSavedList,
            .deleteAllNotification,
            .getProfileInfo,
            .getPlanDetails:
        return .get
//
//            return .put
     
        
        
        }
    }
    
    public var parameters: [String:Any]? {
        switch self {
        case .signUp(let profDet):
            return profDet
        case .logIn(let loginDet):
            return loginDet
        case .getCategory:
            return nil
        case .getSubCategory(let cateId):
            return ["category_id":cateId]
        case .getChildCategory(let cateId):
            return ["category_id":cateId]
        case .getBanner(let details):
            return details
        case .listPost(let details):
            return details
        case .listUserPost:
            return nil
        case .forgotPassword(let email):
            return ["email":email]
        case .updateProfile(let profDet):
            return profDet
        case .changePassword(let profDet):
            return profDet
        case .uploadPost(let profDet):
            return profDet
        case .editPost(let profDet):
            return profDet
        case .getProductDetail(let prodId):
            return ["post_id":prodId]
        case .savePost(let prodId,let save):
            return ["post_id":prodId,"wish_list":save]
        case .writeReview(let profDet):
            return profDet
        case .getSellerProfile(let sellerId):
            return ["user_id":sellerId]
        case .getSavedList:
            return nil
        case .getSubScriptionList:
            return nil
        case .getNotificationList:
            return nil
        case .makeBannerAdd(let prodId):
            return ["post_id":prodId]
        case .makeFeature(let prodId):
            return ["post_id":prodId]
        case .makeSubScribe(let prodId):
            return ["subscription_id":prodId]
        case .removeAllSavedList:
            return nil
        case .deletePost(let prodId):
            return ["post_id":prodId]
        case .deleteNotification(let prodId):
            return ["notification_id":prodId]
        case .deleteAllNotification:
            return nil
        case .addRating(let rating,let sellerId):
            return ["rating":rating,"seller_id":sellerId]
        case .addProductRating(let rating,let productId):
            return ["rating":rating,"product_id":productId]
        case .addNewDevice(let deviceId):
            return ["device_id":deviceId]
        case .getProfileInfo:
            return nil
        case .getPlanDetails:
            return nil
        case .reportComment(let profDet):
            return profDet
        case .deleteComment(let comentId):
            return ["comment_id" : comentId]
        case .subScribeSuccess(let id):
            return ["subscription_id" : id]
        }
    }
    
    public var sampleData: Data {
        switch self {
        case .signUp:
            return stubbedResponse("")
        case .logIn:
            return stubbedResponse("")
        case .getCategory:
            return stubbedResponse("")
        case .getSubCategory:
            return stubbedResponse("")
        case .getChildCategory:
            return stubbedResponse("")
        case .getBanner:
            return stubbedResponse("")
        case .listPost:
            return stubbedResponse("")
        case .listUserPost:
            return stubbedResponse("")
        case .forgotPassword:
            return stubbedResponse("")
        case .updateProfile:
            return stubbedResponse("")
        case .changePassword:
            return stubbedResponse("")
        case .uploadPost:
            return stubbedResponse("")
        case .getProductDetail:
            return stubbedResponse("")
        case .savePost:
            return stubbedResponse("")
        case .writeReview:
            return stubbedResponse("")
        case .getSellerProfile:
            return stubbedResponse("")
        case .editPost:
            return stubbedResponse("")
        case .getSavedList:
            return stubbedResponse("")
        case .getSubScriptionList:
            return stubbedResponse("")
        case .getNotificationList:
            return stubbedResponse("")
        case .makeBannerAdd:
            return stubbedResponse("")
        case .makeFeature:
            return stubbedResponse("")
        case .makeSubScribe:
            return stubbedResponse("")
        case .removeAllSavedList:
            return stubbedResponse("")
        case .deletePost:
            return stubbedResponse("")
        case .deleteNotification:
            return stubbedResponse("")
        case .deleteAllNotification:
            return stubbedResponse("")
        case .addRating:
            return stubbedResponse("")
        case .addProductRating:
            return stubbedResponse("")
        case .addNewDevice:
            return stubbedResponse("")
        case .getProfileInfo:
            return stubbedResponse("")
        case .getPlanDetails:
            return stubbedResponse("")
        case .deleteComment:
            return stubbedResponse("")
        case .reportComment:
            return stubbedResponse("")
        case .subScribeSuccess:
            return stubbedResponse("")
            
        }
    }
    
    public var task: Task {
        switch self {
        case .signUp(let params),.updateProfile(let params),.uploadPost(let params),.editPost(let params):
            var formData = [MultipartFormData]()
            for (key, value) in params {
                if let imgData = value as? Data {
                    formData.append(MultipartFormData(provider: .data(imgData), name: key, fileName: "testImage.jpg", mimeType: "image/jpeg"))
                } else {
                    formData.append(MultipartFormData(provider: .data("\(value)".data(using: .utf8)!), name: key))
                }
            }
            return .uploadMultipart(formData)
        default:
            if(parameters != nil){
                return Task.requestParameters(parameters: (parameters)!, encoding: parameterEncoding)
            }else{
                return Task.requestPlain
            }
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        switch self {
        case  .logIn(_):
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
}

func stubbedResponse(_ filename: String) -> Data! {
    let bundleURL = Bundle.main.bundleURL
    
    let path = bundleURL.appendingPathComponent("Frameworks/ClassifiedAppKit.framework/\(filename).json") //bundle.path(forResource: filename, ofType: "json")
    return (try? Data(contentsOf: path))
}

private extension String {
    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}
