//
//  ClassifiedAPIFacade.swift
//  ClassifiedManager
//
//  Created by islet on 14/12/16.
//  Copyright © 2016 islet. All rights reserved.
//

//
//  ClassifiedAPIFacade.swift
//  Classified
//
//  Created by Isletsystems on 22/09/16.
//  Copyright © 2016 Classified. All rights reserved.
//

import UIKit
import SwiftyJSON

public typealias ClassifiedAPICompletion = (ClassifiedAPIResponse?) -> Void

public class ClassifiedAPIFacade: NSObject {
    struct ClassifiedAPIFacadeConst{
        static let clientId = 2
        static let clientKey = "FEZbPoNvx3vMxP2H1UsP1kCTTVmbxaNlbSuqo7J0"
    }
    public var session : ClassifiedAPISession?
    public class var shared : ClassifiedAPIFacade {
        struct Singleton {
            static let instance = ClassifiedAPIFacade()
        }
        return Singleton.instance
    }
    
    override init() {
        
        ClassifiedAPIProviderFactory.offlineMode = false
        session = ClassifiedAPISession.init()
    }
    
    public func setOfflineMode(_ flag:Bool) {
        ClassifiedAPIProviderFactory.offlineMode = flag
    }
    
    public func signUp(profDet:[String:Any], completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.signUp(profDetails: profDet)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }

    public func login(loginDet:[String:Any], completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.logIn(profDetails: loginDet)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                    if moyaResponse.statusCode == 200 {
                        if YKRes.json?["status"].intValue  == 200{
                            if YKRes.json != nil{
                                self.extractUserSession(YKRes.json!)
                                self.extractUserDetails(YKRes.json!)
                            }
                           
                        }
                    } else {
                        //reset it otherwise
                        self.resetSession()
                    }
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func getCategories(completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.getCategory) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    public func getSubCategories(categoryID:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.getSubCategory(categoryId: categoryID)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func getChildCategory(subCatId:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.getChildCategory(subCategoryId: subCatId)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func getBannerImages(details:[String:Any],completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.getBanner(profDetails: details)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func lisPosts(details:[String:Any],completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.listPost(profDetails: details)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func forgotPassword(email:String,completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.forgotPassword(email: email)) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    public func listUserPosts(completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.listUserPost) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    public func updateProfile(details:[String:Any],completion:ClassifiedAPICompletion?) {
              ClassifiedAPIProviderFactory.sharedProvider.request(.updateProfile(profDetails: details)) { result in
                  switch result {
                  case let .success(moyaResponse):
                      let jsonData = moyaResponse.data
                      if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                          YKRes.statusCode = moyaResponse.statusCode
                            if YKRes.json?["status"].intValue  == 200{
                                if YKRes.json != nil{
                                    self.extractUserDetails(YKRes.json!)
                                }
                            }
                          completion?(YKRes)
                          return
                      }
                  case .failure(_):
                      completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                  }
              }
          }
    
    public func chnagePassword(details:[String:Any],completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.changePassword(profDetails: details)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                     
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func getSellerProdile(sellerID:Int,completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.getSellerProfile(sellerID: sellerID)) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                        
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    public func uploadPost(details:[String:Any],completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.uploadPost(profDetails: details)) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                        
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    public func getSavedList(completion:ClassifiedAPICompletion?) {
              ClassifiedAPIProviderFactory.sharedProvider.request(.getSavedList) { result in
                  switch result {
                  case let .success(moyaResponse):
                      let jsonData = moyaResponse.data
                      if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                          YKRes.statusCode = moyaResponse.statusCode
                           
                          completion?(YKRes)
                          return
                      }
                  case .failure(_):
                      completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                  }
              }
          }
    
    public func getSubscriptionList(completion:ClassifiedAPICompletion?) {
                ClassifiedAPIProviderFactory.sharedProvider.request(.getSubScriptionList) { result in
                    switch result {
                    case let .success(moyaResponse):
                        let jsonData = moyaResponse.data
                        if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                            YKRes.statusCode = moyaResponse.statusCode
                             
                            completion?(YKRes)
                            return
                        }
                    case .failure(_):
                        completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                    }
                }
            }
    
    public func ediPost(details:[String:Any],completion:ClassifiedAPICompletion?) {
              ClassifiedAPIProviderFactory.sharedProvider.request(.editPost(profDetails: details)) { result in
                  switch result {
                  case let .success(moyaResponse):
                      let jsonData = moyaResponse.data
                      if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                          YKRes.statusCode = moyaResponse.statusCode
                           
                          completion?(YKRes)
                          return
                      }
                  case .failure(_):
                      completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                  }
              }
          }
    
     public func getnotificationList(completion:ClassifiedAPICompletion?) {
               ClassifiedAPIProviderFactory.sharedProvider.request(.getNotificationList) { result in
                   switch result {
                   case let .success(moyaResponse):
                       let jsonData = moyaResponse.data
                       if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                           YKRes.statusCode = moyaResponse.statusCode
                            
                           completion?(YKRes)
                           return
                       }
                   case .failure(_):
                       completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                   }
               }
           }
    
    public func makeBannerAd(postID:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.makeBannerAdd(prodId:postID)) { result in
                     switch result {
                     case let .success(moyaResponse):
                         let jsonData = moyaResponse.data
                         if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                             YKRes.statusCode = moyaResponse.statusCode
                              
                             completion?(YKRes)
                             return
                         }
                     case .failure(_):
                         completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                     }
                 }
             }
    public func makeFeatureAd(postID:Int,completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.makeFeature(prodId:postID)) { result in
                        switch result {
                        case let .success(moyaResponse):
                            let jsonData = moyaResponse.data
                            if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                                YKRes.statusCode = moyaResponse.statusCode
                                 
                                completion?(YKRes)
                                return
                            }
                        case .failure(_):
                            completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                        }
                    }
                }
    
    public func makeSubscribe(subScribeId:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.makeSubScribe(prodId: subScribeId)) { result in
                 switch result {
                 case let .success(moyaResponse):
                     let jsonData = moyaResponse.data
                     if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                         YKRes.statusCode = moyaResponse.statusCode
                          
                         completion?(YKRes)
                         return
                     }
                 case .failure(_):
                     completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                 }
             }
         }
    
    public func getProductDetails(prodID:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.getProductDetail(productID: prodID)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                     
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func savePost(prodID:Int,save:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.savePost(productID: prodID, save: save)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                     
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func writeReview(details:[String:Any],completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.writeReview(profDetails: details)) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                        
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    
    public func removrAllSavedList(completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.removeAllSavedList) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                        
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    public func deletePost(postID:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.deletePost(prodId: postID)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                     
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func deleteNotification(notfcId:Int,completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.deleteNotification(notfcnId: notfcId)) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                        
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    
       public func deleteAllNotification(completion:ClassifiedAPICompletion?) {
              ClassifiedAPIProviderFactory.sharedProvider.request(.deleteAllNotification) { result in
                  switch result {
                  case let .success(moyaResponse):
                      let jsonData = moyaResponse.data
                      if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                          YKRes.statusCode = moyaResponse.statusCode
                           
                          completion?(YKRes)
                          return
                      }
                  case .failure(_):
                      completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                  }
              }
          }
    

    public func addRating(rating:Int,sellerID:Int,completion:ClassifiedAPICompletion?) {
           ClassifiedAPIProviderFactory.sharedProvider.request(.addRating(rating: rating, sellerID: sellerID)) { result in
               switch result {
               case let .success(moyaResponse):
                   let jsonData = moyaResponse.data
                   if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                       YKRes.statusCode = moyaResponse.statusCode
                        
                       completion?(YKRes)
                       return
                   }
               case .failure(_):
                   completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
               }
           }
       }
    
    public func addProductRating(rating:Int,productId:Int,completion:ClassifiedAPICompletion?) {
              ClassifiedAPIProviderFactory.sharedProvider.request(.addProductRating(rating: rating, productId: productId)) { result in
                  switch result {
                  case let .success(moyaResponse):
                      let jsonData = moyaResponse.data
                      if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                          YKRes.statusCode = moyaResponse.statusCode
                           
                          completion?(YKRes)
                          return
                      }
                  case .failure(_):
                      completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                  }
              }
          }
    
    public func addNewDevice(deviceId:String,completion:ClassifiedAPICompletion?) {
                 ClassifiedAPIProviderFactory.sharedProvider.request(.addNewDevice(deviceID: deviceId)) { result in
                     switch result {
                     case let .success(moyaResponse):
                         let jsonData = moyaResponse.data
                         if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                             YKRes.statusCode = moyaResponse.statusCode
                              
                             completion?(YKRes)
                             return
                         }
                     case .failure(_):
                         completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
                     }
                 }
             }
    
    public func getProfileInfo(completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.getProfileInfo) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                     if moyaResponse.statusCode == 200 {
                         if YKRes.json?["status"].intValue  == 200{
                             if YKRes.json != nil{
                                 self.extractUserDetails(YKRes.json!)
                             }
                         }
                     }
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func getPlanDetails(completion:ClassifiedAPICompletion?) {
          ClassifiedAPIProviderFactory.sharedProvider.request(.getPlanDetails) { result in
              switch result {
              case let .success(moyaResponse):
                  let jsonData = moyaResponse.data
                  if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                      YKRes.statusCode = moyaResponse.statusCode
                       if moyaResponse.statusCode == 200 {
                           if YKRes.json?["status"].intValue  == 200{
                               if YKRes.json != nil{
                                   self.extractUserDetails(YKRes.json!)
                               }
                           }
                       }
                      completion?(YKRes)
                      return
                  }
              case .failure(_):
                  completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
              }
          }
      }
    
    public func reportcomment(details:[String:Any],completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.reportComment(profDetails: details)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                     if moyaResponse.statusCode == 200 {
                         if YKRes.json?["status"].intValue  == 200{
                             if YKRes.json != nil{
                                 self.extractUserDetails(YKRes.json!)
                             }
                         }
                     }
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }
    
    public func deleteComment(commentID:Int,completion:ClassifiedAPICompletion?) {
          ClassifiedAPIProviderFactory.sharedProvider.request(.deleteComment(commentId: commentID)) { result in
              switch result {
              case let .success(moyaResponse):
                  let jsonData = moyaResponse.data
                  if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                      YKRes.statusCode = moyaResponse.statusCode
                       if moyaResponse.statusCode == 200 {
                           if YKRes.json?["status"].intValue  == 200{
                               if YKRes.json != nil{
                                   self.extractUserDetails(YKRes.json!)
                               }
                           }
                       }
                      completion?(YKRes)
                      return
                  }
              case .failure(_):
                  completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
              }
          }
      }
    
    public func subScribeSuccess(subScribeId:Int,completion:ClassifiedAPICompletion?) {
        ClassifiedAPIProviderFactory.sharedProvider.request(.subScribeSuccess(subScribeId: subScribeId)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonData = moyaResponse.data
                if let YKRes = ClassifiedAPIResponse.fromJSONData(jsonData) as? ClassifiedAPIResponse {
                    YKRes.statusCode = moyaResponse.statusCode
                     if moyaResponse.statusCode == 200 {
                         if YKRes.json?["status"].intValue  == 200{
                             if YKRes.json != nil{
                                 self.extractUserDetails(YKRes.json!)
                             }
                         }
                     }
                    completion?(YKRes)
                    return
                }
            case .failure(_):
                completion?(ClassifiedAPIResponse(success:false, data: nil, json: nil))
            }
        }
    }

    public func resetSession() {
        self.session?.token = nil
    }
    
    public func extractUserSession(_ json:JSON) {
        let datatype = json["data"]
        self.session?.token = datatype["token"].stringValue
    }
    
    public func extractUserDetails(_ json:JSON) {
        let user = User(json:json["data"])
        self.session?.currentUser = user
    }

    // MARK: - image path
    public func urlForImage(imagePath:String) -> URL?{
        let path = ClassifiedAPIConfig.BaseUrl.baseServerpath + "uploads/" + imagePath
        return URL(string: path)
    }
}


