//
//  Notification.swift
//  ClassifiedWebkit
//
//  Created by WC_Macmini on 25/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyJSON

public class Notifications: JSONAble {
        public var id : Int?
       public var text : String?
       public var post_id : Int?
       public var photo : String?
       public var time : String?

           
       public init(json:JSON) {
           id = json["id"].int
           text = json["text"].string
           post_id = json["post_id"].stringSafeInt()
           photo  = json["photo"].string
            time  = json["time"].string
       }
}
