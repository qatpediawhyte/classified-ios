//
//  File.swift
//  ClassifiedWebkit
//
//  Created by WC_Macmini on 18/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyJSON

public class User: JSONAble {
    public var userId : Int?
    public var userName : String?
    public var image : String?
    public var emailId : String?
    public var phone : String?
    public var mamber_since : String?
    public var rating : String?
    public var active_subscription : Int?
        
    public init(json:JSON) {
        userId = json["id"].int
        userName = json["name"].string
        image = json["photo"].string
        emailId = json["email"].string
        phone = json["phone"].intToSafeString()
        mamber_since = json["mamber_since"].string
        rating = json["rating"].intToSafeString()
        active_subscription = json["active_subscription"].int
  }
        
    public func dicValue() -> [String:Any] {
        var userDic = [String:Any]()
        userDic["id"] = userId
        userDic["name"] = userName
        userDic["photo"] = image
        userDic["email"] = emailId
        userDic["phone"] = phone
        userDic["mamber_since"] = mamber_since
        userDic["rating"] = rating
        userDic["active_subscription"] = active_subscription
        return userDic
    }
}
    



