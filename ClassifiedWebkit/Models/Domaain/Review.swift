//
//  Review.swift
//  ClassifiedWebkit
//
//  Created by WC_Macmini on 20/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyJSON

public class Review: JSONAble {
    public var review : String?
    public var time_ago : String?
    public var user_name : String?
    public var user_photo : String?
    public var user_id : Int?
    public var id : Int?
    
         
public init(json:JSON) {
    id = json["id"].int
    review = json["review"].string
    time_ago = json["time_ago"].string
    user_name = json["user_name"].string
    user_photo  = json["user_photo"].string
    user_id  = json["user_id"].int
}
}
