//
//  Post.swift
//  ClassifiedWebkit
//
//  Created by WC_Macmini on 19/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyJSON

public class Post: JSONAble {
    public var post_id : Int?
    public var name : String?
    public var name_ar : String?
    public var photo : String?
    public var price : String?
    public var previous_price : String?
    public var user_id : Int?
    public var user_name : String?
    public var phone_number : String?
    public var time_agos : String?
    public var wish_list : Int?
    public var product_condition : Int?
    
    
         
    public init(json:JSON) {
        post_id = json["post_id"].int
        name = json["name"].string
        name_ar = json["name_ar"].string
        photo = json["photo"].string
        price = json["price"].intToSafeString()
        previous_price = json["previous_price"].intToSafeString()
        user_id  = json["posted_user_id"].int
        user_name = json["posted_user_name"].string
        phone_number = json["posted_phone_number"].intToSafeString()
        time_agos = json["time_ago"].string
        wish_list = json["wish_list"].int ?? 0
        product_condition = json["product_condition"].stringSafeInt()
    }
}
