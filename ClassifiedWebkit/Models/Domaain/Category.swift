//
//  Category.swift
//  ClassifiedWebkit
//
//  Created by WC_Macmini on 19/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyJSON

public class Categorys: JSONAble {
    public var id : Int?
    public var name : String?
    public var name_ar : String?
    public var slug : String?
    public var photo : String?
    public var sub_cat_count : Int?
    public var child_cat_count : Int?
    
           
    public init(json:JSON) {
        id = json["id"].int
        name = json["name"].string
        name_ar = json["name_ar"].string
        slug = json["slug"].string
        photo = json["photo"].intToSafeString()
        sub_cat_count = json["sub_cat_count"].int
        child_cat_count = json["child_cat_count"].int
    }
}
