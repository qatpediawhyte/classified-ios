//
//  Banner.swift
//  ClassifiedWebkit
//
//  Created by WC_Macmini on 19/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyJSON

public class Banner: JSONAble {
    public var id : Int?
    public var product_id : Int?
    public var photo : String?
         
    public init(json:JSON) {
        id = json["id"].int
        product_id = json["product_id"].stringSafeInt()
        photo = json["photo"].intToSafeString()
    }
}
