//
//  Subscription.swift
//  ClassifiedWebkit
//
//  Created by WC_Macmini on 23/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import SwiftyJSON

public class Subscription: JSONAble {
    public var id : Int?
    public var title : String?
    public var price : String?
    public var days : String?
    public var allowed_products : String?
    public var allowed_banner_products : String?
    public var details : String?
        
    public init(json:JSON) {
        id = json["id"].int
        title = json["title"].string
        price = json["price"].intToSafeString()
        days  = json["days"].intToSafeString()
        allowed_products  = json["allowed_products"].intToSafeString()
        allowed_banner_products  = json["allowed_banner_products"].intToSafeString()
        details  = json["details"].intToSafeString()
    }
}
